package extensions;

import org.openqa.selenium.WebElement;

public class Is {
    public static boolean visible (WebElement webElement) {
        try{
            return webElement.isDisplayed();
        } catch (Exception ignore) {
            return false;
        }
    }
}
