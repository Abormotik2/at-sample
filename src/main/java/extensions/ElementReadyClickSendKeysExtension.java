package extensions;

import io.qameta.atlas.core.api.MethodExtension;
import io.qameta.atlas.core.internal.Configuration;
import io.qameta.atlas.core.internal.TargetMethodInvoker;
import io.qameta.atlas.core.util.MethodInfo;
import io.qameta.atlas.webdriver.AtlasWebElement;
import io.qameta.atlas.webdriver.context.WebDriverContext;
import org.openqa.selenium.remote.RemoteWebDriver;
import ru.vtb.at.pages.element.UIElement;
import utils.StyleResourceHelper;
import utils.Wait;

import java.lang.reflect.Method;
import java.util.Arrays;

import static ru.yandex.qatools.matchers.webdriver.EnabledMatcher.enabled;

public class ElementReadyClickSendKeysExtension implements MethodExtension {
    private final String[] METHOD_NAMES = {
            "click",
            "sendKeys",
            "getText",
    };

    @Override
    public Object invoke(Object proxy, MethodInfo methodInfo, Configuration configuration) throws Throwable {
        //Script add addMouseTracker
        if (configuration.getContext(WebDriverContext.class).isPresent()) {
            if (((RemoteWebDriver) configuration.getContext(WebDriverContext.class).get().getValue())
                    .getCapabilities().getBrowserName().equalsIgnoreCase("chrome")) {
                StyleResourceHelper.addMouseTracker();
            }
        }

        UIElement uiElement = (UIElement) proxy;
        Wait.until(uiElement::isDisplayed, 10, 1);
        return new TargetMethodInvoker().invoke(uiElement, methodInfo, configuration);
    }

    @Override
    public boolean test(Method method) {
        return (Arrays.stream(METHOD_NAMES).
                anyMatch(s -> s.equalsIgnoreCase(method.getName()))
                && AtlasWebElement.class == method.getDeclaringClass());
    }
}
