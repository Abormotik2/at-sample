package extensions;

import io.qameta.allure.Allure;
import io.qameta.allure.AllureLifecycle;
import io.qameta.allure.model.Status;
import io.qameta.allure.model.StepResult;
import io.qameta.allure.util.ResultsUtils;
import io.qameta.atlas.core.api.Listener;
import io.qameta.atlas.core.api.Target;
import io.qameta.atlas.core.context.TargetContext;
import io.qameta.atlas.core.internal.Configuration;
import io.qameta.atlas.core.util.MethodInfo;
import org.hamcrest.Matcher;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * Расширение для Atlas, для полученния более детальной и о взаимодействия с web элементами.
 */
public class AllureAtlasListener implements Listener {

    private final AllureLifecycle lifecycle = Allure.getLifecycle();
    private final Map<String, MethodFormatter> loggableMethods;

    public AllureAtlasListener() {
        loggableMethods = new HashMap<>();
        loggableMethods.put("click", (name, args) -> String.format("Нажать на элемент \'%s\'", name));

        loggableMethods.put("getText", (name, args) -> String.format("Получение текста элемента  \'%s\'", name));
        loggableMethods.put("isDisplayed", (name, args) -> String.format("Получение состояния видимости элемента \'%s\' на странице.", name));
        loggableMethods.put("isEnabled", (name, args) -> String.format("Получение состояния доступности элемента \'%s\' на странице.", name));
        loggableMethods.put("clear", (name, args) -> String.format("Очищается поле  \'%s\'", name));
        loggableMethods.put("sendKeys", (name, args) -> {
            String arguments = (args[0] instanceof CharSequence[] ? Arrays.toString(((CharSequence[]) args[0])) : String.valueOf(args[0]));
            return String.format("Ввод в элемент  \'%s\' значения:  [%s]", name, arguments);
        });
        loggableMethods.put("filter", (name, args) -> String.format("Поиск подходящего элемента из коллекции элементов \'%s\'", name));
        loggableMethods.put("waitUntil", (name, args) -> {
            Matcher matcher = (Matcher) (args[0] instanceof Matcher ? args[0] : args[1]);
            return String.format("Подождать пока  состояние элемента \'%s\' не будет удовлетворять требуемому: [%s]", name, matcher);
        });
        loggableMethods.put("should", (description, args) -> {
            Matcher matcher = (Matcher) (args[0] instanceof Matcher ? args[0] : args[1]);
            return String.format("Дождаться когда требуемое состояние элемента  \'%s\'  будет удовлетворять: [%s]", description, matcher);
        });
    }

    @Override
    public void beforeMethodCall(final MethodInfo methodInfo,
                                 final Configuration configuration) {
        getMethodFormatter(methodInfo.getMethod()).ifPresent(formatter -> {
            final String name = configuration.getContext(TargetContext.class)
                    .map(TargetContext::getValue)
                    .map(Target::name)
                    .orElse(methodInfo.getMethod().getName());
            final Object[] args = methodInfo.getArgs();
            lifecycle.startStep(Objects.toString(methodInfo.hashCode()),
                    new StepResult().setName(formatter.format(name, args)).setStatus(Status.PASSED));
        });
    }

    @Override
    public void afterMethodCall(final MethodInfo methodInfo,
                                final Configuration configuration) {
        getMethodFormatter(methodInfo.getMethod())
                .ifPresent(title -> lifecycle.stopStep(Objects.toString(methodInfo.hashCode())));
    }

    @Override
    public void onMethodReturn(final MethodInfo methodInfo,
                               final Configuration configuration,
                               final Object returned) {
    }

    @Override
    public void onMethodFailure(final MethodInfo methodInfo,
                                final Configuration configuration,
                                final Throwable throwable) {
        getMethodFormatter(methodInfo.getMethod()).ifPresent(title ->
                lifecycle.updateStep(stepResult -> {
                    stepResult.setStatus(ResultsUtils.getStatus(throwable).orElse(Status.BROKEN));
                    stepResult.setStatusDetails(ResultsUtils.getStatusDetails(throwable).orElse(null));
                })
        );
    }

    private Optional<MethodFormatter> getMethodFormatter(Method method) {
        return Optional.ofNullable(loggableMethods.get(method.getName()));
    }


    @FunctionalInterface
    private interface MethodFormatter {
        String format(String name, Object[] args);
    }

}