package exceptions.kubernetes;

public class SecretsException extends RuntimeException {

    public SecretsException() {
    }

    public SecretsException(String message) {
        super(message);
    }

    public SecretsException(String message, Throwable cause) {
        super(message, cause);
    }

    public SecretsException(Throwable cause) {
        super(cause);
    }

    public SecretsException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
