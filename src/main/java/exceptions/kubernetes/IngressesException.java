package exceptions.kubernetes;

public class IngressesException extends RuntimeException {
    public IngressesException() {
    }

    public IngressesException(String message) {
        super(message);
    }

    public IngressesException(String message, Throwable cause) {
        super(message, cause);
    }

    public IngressesException(Throwable cause) {
        super(cause);
    }

    public IngressesException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
