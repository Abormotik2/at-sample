package deprecated.api.services;

import com.consol.citrus.dsl.endpoint.CitrusEndpoints;
import com.consol.citrus.http.client.HttpClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import utils.kubernetes.Kubernetes;
@Deprecated
@Configuration
@Lazy
public class AuthService {
    @Bean(name = "auth-service")
    public HttpClient authService() {
        return CitrusEndpoints
                .http()
                .client()
                .requestUrl(Kubernetes.ingresses.getUrl("auth"))
                .build();
    }
}
