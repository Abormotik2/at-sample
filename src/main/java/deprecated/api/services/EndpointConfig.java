
package deprecated.api.services;

import com.consol.citrus.TestCase;
import com.consol.citrus.context.TestContext;
import com.consol.citrus.dsl.endpoint.CitrusEndpoints;
import com.consol.citrus.exceptions.CitrusRuntimeException;
import com.consol.citrus.kubernetes.client.KubernetesClient;
import com.consol.citrus.message.Message;
import com.consol.citrus.report.MessageTracingTestListener;
import org.apache.commons.dbcp.BasicDataSource;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import ru.vtb.at.Config;
import utils.allure.AllureHelper;


@Configuration
@Lazy
@ComponentScan({"api.services"})
public class EndpointConfig {
    private static final Logger LOG = LogManager.getLogger(EndpointConfig.class);
    String namespace = Config.getStringSystemProperty("namespace");
    String kuberUrl = Config.getStringSystemProperty("kuber.url");


    @Bean(name = "kuber")
    public KubernetesClient kuber() {
        return CitrusEndpoints
                .kubernetes()
                .client()
                .namespace(namespace)
                .url(kuberUrl)
                .build();
    }

    @Bean(name = "messageTracingTestListener")
    public MessageTracingTestListener messageTracingTestListener() {
        return new CustomMessageListener();
    }

    private static class CustomMessageListener extends MessageTracingTestListener {
        private final StringBuilder stringBuilder = new StringBuilder();

        @Override
        public void onInboundMessage(Message message, TestContext context) {
            stringBuilder.append("INBOUND_MESSAGE:").append(newLine()).append(message).append(newLine()).append(separator()).append(newLine());
            AllureHelper.attachTxt("ответ на исходящее сообщение", message.toString());
            super.onInboundMessage(message, context);
            LOG.info("ПОЛУЧЕН ОТВЕТ: \n {} \n", message.toString());
        }

        @Override
        public void onOutboundMessage(Message message, TestContext context) {
            stringBuilder.append("OUTBOUND_MESSAGE:").append(newLine()).append(message).append(newLine()).append(separator()).append(newLine());
            AllureHelper.attachTxt("исходящее сообщение", message.toString());
            super.onInboundMessage(message, context);
            LOG.info("ОТПРАВЛЕН ЗАПРОС: \n {} \n", message.toString());
        }

        @Override
        public void afterPropertiesSet() throws Exception {
            try {
                super.afterPropertiesSet();
            } catch (CitrusRuntimeException ignore) {
            }
        }

        private String newLine() {
            return "\n";
        }

        @Override
        public void onTestFinish(TestCase test) {
            super.onTestFinish(test);
            AllureHelper.attachTxt("Запросы теста", stringBuilder.toString());
            stringBuilder.setLength(0);
        }

        private String separator() {
            return "======================================================================";
        }
    }

    /**
     * bean-описание для создания подключения к бд из параметров описанных в файле @See default.property
     *
     * @return springBean, который доступен из ApplicationContext
     */

    @Bean(name = "postgresql")
    public BasicDataSource testDBPostgresql() {
        BasicDataSource dataSource = new BasicDataSource();
        dataSource.setDriverClassName(Config.getStringSystemProperty("postgresql.driver"));
        dataSource.setUrl(Config.getStringSystemProperty("postgresql.url"));
        dataSource.setUsername(Config.getStringSystemProperty("postgresql.user"));
        dataSource.setPassword(Config.getStringSystemProperty("postgresql.pass"));
        dataSource.setInitialSize(3);
        dataSource.setMaxActive(3);
        dataSource.setMaxIdle(2);
        return dataSource;
    }
}
