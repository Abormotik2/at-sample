package deprecated.api.services.business.test;

import java.util.Date;

public interface CertTemplate {

    public String getCert_name();

    public String getUser_name();

    public Date getLocked();

    public String getUid();

    public String getPublic_key();

    public String getPrivate_key();


}
