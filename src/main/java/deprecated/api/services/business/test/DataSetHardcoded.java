package deprecated.api.services.business.test;


import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

/**
 * класс-описание таблицы из дп
 */

public class DataSetHardcoded {
    String org_cabs_id;
    String full_name;
    String short_name;
    String ogrn;
    String okpo;
    String inn;
    String kpp;
    String kpp_big_tax_payer;
    String okato;
    String account_cabs_id;
    String account_name;
    String account_currency_alphcode;
    String account_currency_isocode;
    String account_number;
    String open_date;
    String headbranch_cabs_id;
    String headbranch_tfu;
    String bic;
    String headbranch_full_name;
    String headbranch_short_name;
    String headbranch_code;
    String branch_cabs_id;
    String branch_parent;
    String branch_category;
    String branch_tfu;
    String branch_full_name;
    String branch_short_name;
    String branch_code;
    String branch_time_zone;
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSSS")
    Date locked;

    public String getOrg_cabs_id() {
        return org_cabs_id;
    }

    public String getFull_name() {
        return full_name;
    }

    public String getShort_name() {
        return short_name;
    }

    public String getOgrn() {
        return ogrn;
    }

    public String getOkpo() {
        return okpo;
    }

    public String getInn() {
        return inn;
    }

    public String getKpp() {
        return kpp;
    }

    public String getKpp_big_tax_payer() {
        return kpp_big_tax_payer;
    }

    public String getOkato() {
        return okato;
    }

    public String getAccount_cabs_id() {
        return account_cabs_id;
    }

    public String getAccount_name() {
        return account_name;
    }

    public String getAccount_currency_alphcode() {
        return account_currency_alphcode;
    }

    public String getAccount_currency_isocode() {
        return account_currency_isocode;
    }

    public String getAccount_number() {
        return account_number;
    }

    public String getOpen_date() {
        return open_date;
    }

    public String getHeadbranch_cabs_id() {
        return headbranch_cabs_id;
    }

    public String getHeadbranch_tfu() {
        return headbranch_tfu;
    }

    public String getBic() {
        return bic;
    }

    public String getHeadbranch_full_name() {
        return headbranch_full_name;
    }

    public String getHeadbranch_short_name() {
        return headbranch_short_name;
    }

    public String getHeadbranch_code() {
        return headbranch_code;
    }

    public String getBranch_cabs_id() {
        return branch_cabs_id;
    }

    public String getBranch_parent() {
        return branch_parent;
    }

    public String getBranch_category() {
        return branch_category;
    }

    public String getBranch_tfu() {
        return branch_tfu;
    }

    public String getBranch_full_name() {
        return branch_full_name;
    }

    public String getBranch_short_name() {
        return branch_short_name;
    }

    public String getBranch_code() {
        return branch_code;
    }

    public String getBranch_time_zone() {
        return branch_time_zone;
    }

    public Date getLocked() {
        return locked;
    }
}
