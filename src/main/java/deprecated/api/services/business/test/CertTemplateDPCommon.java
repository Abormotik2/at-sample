package deprecated.api.services.business.test;



import java.util.Date;

/**
 * класс описание таблички,
 * в которой лежат свободные/залоченные сертификаты в dp
 */

public class CertTemplateDPCommon implements Unlockable, CertTemplate {

    //поля
    String cert_name;
    String user_name;
    Date locked;
    String uid;
    String public_key;
    String private_key;


    public String getCert_name() {
        return cert_name;
    }

    public String getUser_name() {
        return user_name;
    }


    public Date getLocked() {
        return locked;
    }

    public void setCert_name(String cert_name) {
        this.cert_name = cert_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public void setLocked(Date locked) {
        this.locked = locked;
    }

    public String getUid() {
        return uid;
    }

    public String getPublic_key() {
        return public_key;
    }

    public String getPrivate_key() {
        return private_key;
    }

}