package deprecated.api.services.business.test;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

/**
 * класс-описание таблицы из дп
 */
public class Department {

    Integer aff_bic;//42202837,
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSSS")  // поставил дефолтный паттерн для колонки с дата-типом, почти вся колонка заполнена NULL
    Date validtodate;//null,
    Integer aff_cabs_id;//1240327669,
    Integer sysfilial;//24,
    String aff_fname;//"ФИЛИАЛ БАНКА ВТБ (ПАО) В Г.НИЖНЕМ НОВГОРОДЕ",
    String aff_sname;//"ФИЛИАЛ БАНКА ВТБ (ПАО) В Г.НИЖНЕМ НОВГОРОДЕ",
    String aff_code;//"724000",
    String aff_tfo;//"2400",
    Integer dep_cabs_id;//1240327835,
    Integer dep_parent;//1240327669,
    String dep_category;//"ДОПОЛНИТЕЛЬНЫЙ ОФИС",
    String dep_fname;//"Дополнительный офис \"АЭРОПОРТ\"",
    String dep_sname;//"Дополнительный офис \"АЭРОПОРТ\"",
    String dep_code;//"724401",
    String dep_tfo;//"2401",
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSSS")   // поставил дефолтный паттерн для колонки с дата-типом, почти вся колонка заполнена NULL
    Date dep_time;//null

    public void setAff_bic(Integer aff_bic) {
        this.aff_bic = aff_bic;
    }

    public void setValidtodate(Date validtodate) {
        this.validtodate = validtodate;
    }

    public void setAff_cabs_id(Integer aff_cabs_id) {
        this.aff_cabs_id = aff_cabs_id;
    }

    public void setSysfilial(Integer sysfilial) {
        this.sysfilial = sysfilial;
    }

    public void setAff_fname(String aff_fname) {
        this.aff_fname = aff_fname;
    }

    public void setAff_sname(String aff_sname) {
        this.aff_sname = aff_sname;
    }

    public void setAff_code(String aff_code) {
        this.aff_code = aff_code;
    }

    public void setAff_tfo(String aff_tfo) {
        this.aff_tfo = aff_tfo;
    }

    public void setDep_cabs_id(Integer dep_cabs_id) {
        this.dep_cabs_id = dep_cabs_id;
    }

    public void setDep_parent(Integer dep_parent) {
        this.dep_parent = dep_parent;
    }

    public void setDep_category(String dep_category) {
        this.dep_category = dep_category;
    }

    public void setDep_fname(String dep_fname) {
        this.dep_fname = dep_fname;
    }

    public void setDep_sname(String dep_sname) {
        this.dep_sname = dep_sname;
    }

    public void setDep_code(String dep_code) {
        this.dep_code = dep_code;
    }

    public void setDep_tfo(String dep_tfo) {
        this.dep_tfo = dep_tfo;
    }

    public void setDep_time(Date dep_time) {
        this.dep_time = dep_time;
    }

    public Integer getAff_bic() {
        return aff_bic;
    }

    public Date getValidtodate() {
        return validtodate;
    }

    public Integer getAff_cabs_id() {
        return aff_cabs_id;
    }

    public Integer getSysfilial() {
        return sysfilial;
    }

    public String getAff_fname() {
        return aff_fname;
    }

    public String getAff_sname() {
        return aff_sname;
    }

    public String getAff_code() {
        return aff_code;
    }

    public String getAff_tfo() {
        return aff_tfo;
    }

    public Integer getDep_cabs_id() {
        return dep_cabs_id;
    }

    public Integer getDep_parent() {
        return dep_parent;
    }

    public String getDep_category() {
        return dep_category;
    }

    public String getDep_fname() {
        return dep_fname;
    }

    public String getDep_sname() {
        return dep_sname;
    }

    public String getDep_code() {
        return dep_code;
    }

    public String getDep_tfo() {
        return dep_tfo;
    }

    public Date getDep_time() {
        return dep_time;
    }
}
