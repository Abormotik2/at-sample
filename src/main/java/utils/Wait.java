package utils;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.vtb.at.context.Context;
import ru.vtb.at.driver.DriverManager;
import ru.vtb.at.pages.element.UIElement;

import java.util.function.Supplier;

public class Wait {

    public static final int PAUSE = 1;
    public static final int TIMEOUT = 5;

    public static void until(Supplier<Boolean> waitingCondition) {
        until(waitingCondition, TIMEOUT, PAUSE);
    }

    public static void until(Supplier<Boolean> waitingCondition, int timeout, int pause) {
        long startTime = System.currentTimeMillis();
        long endTime = startTime + (long) (timeout * 1000);
        while (!waitingCondition.get() && System.currentTimeMillis() < endTime) {
            try {
                Thread.sleep(pause * 1000);
            } catch (InterruptedException e) {
            }
        }
    }

    public static UIElement waitToBeClicable(UIElement element) {
        new WebDriverWait(Context.getInstance().getBean(DriverManager.class).getDriver(), 60)
                .until(ExpectedConditions.elementToBeClickable(element));
        return element;
    }
}
