package utils.kubernetes.services;

import com.consol.citrus.kubernetes.client.KubernetesClient;
import exceptions.kubernetes.ServicesException;
import io.fabric8.kubernetes.api.model.Service;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Вспомогательный класс для работы с service kubernetes.
 */

public class ServicesHelper {
    private static Logger LOGGER = LogManager.getLogger(ServicesHelper.class);
    private KubernetesClient kubernetes;


    public ServicesHelper(KubernetesClient client) {
        this.kubernetes = client;
    }


    /**
     * Получить сервис по имени в метаданных сервиса.
     *
     * @param serviceName - имя сервиса
     * @return Service сервис
     */
    public Service getServiceByName(String serviceName) {
        LOGGER.info("\nKubernetes, поиск сервиса :" + serviceName);
        return kubernetes.getClient().services().list().getItems().stream().
                filter(service -> service.getMetadata().getName().equalsIgnoreCase(serviceName)).findFirst()
                .orElseThrow(() -> new ServicesException("Не найден сервис с именем:'" + serviceName + "'"));

    }

}
