package utils.kubernetes;

import com.consol.citrus.kubernetes.client.KubernetesClient;
import ru.vtb.at.citrus.CitrusRunner;
import ru.vtb.at.context.Context;
import utils.kubernetes.endpoints.EndpointsHeplper;
import utils.kubernetes.ingresses.IngressesHelper;
import utils.kubernetes.pods.PodsHelper;
import utils.kubernetes.secrets.SecretsHelper;
import utils.kubernetes.services.ServicesHelper;


/**
 * Класс через который реализована работа с Kubernetes
 */
public class Kubernetes {

    static KubernetesClient kubernetesClient = (KubernetesClient) Context.getInstance().getBean(CitrusRunner.class).
            getTestContext().getApplicationContext().getBean("kuber");


    public static IngressesHelper ingresses = new IngressesHelper(kubernetesClient);
    public static EndpointsHeplper endpointsHeplper = new EndpointsHeplper(kubernetesClient);
    public static ServicesHelper servicesHeplper = new ServicesHelper(kubernetesClient);
    public static SecretsHelper secretsHeplper = new SecretsHelper(kubernetesClient);
    public static PodsHelper podsHelper = new PodsHelper(kubernetesClient);


}
