package utils.kubernetes.endpoints;

import com.consol.citrus.kubernetes.client.KubernetesClient;
import exceptions.kubernetes.EndpointException;
import exceptions.kubernetes.ServicesException;
import io.fabric8.kubernetes.api.model.Endpoints;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Вспомогательный класс для работы с endpoints kubernetes.
 */

public class EndpointsHeplper {
    private static Logger LOGGER = LogManager.getLogger(EndpointsHeplper.class);
    private KubernetesClient kubernetes;


    public EndpointsHeplper(KubernetesClient client) {
        this.kubernetes = client;
    }


    /**
     * Получить эндопоиинт по имени в метаданных энжопинта.
     *
     * @param endpointName - имя эндпоинта
     * @return Endpoints эндопинт
     */
    public Endpoints getEndpointsByName(String endpointName) {
        LOGGER.info("\nKubernetes, поиск эндпоинта :" + endpointName);
        return kubernetes.getClient().endpoints().list().getItems().stream().
                filter(endpoints -> endpoints.getMetadata().getName().equalsIgnoreCase(endpointName)).findFirst()
                .orElseThrow(() -> new ServicesException("Не найден эндпоинт с именем:'" + endpointName + "'"));

    }


    /**
     * Получить ip адресс эндпоинта по имени в метаданных энжопинта.
     *
     * @param endpointName - имя эндпоинта
     * @return host ip адресс эндпоинта
     */

    public String getHost(String endpointName) {
        LOGGER.info("\nKubernetes, поиск хоста  по имени эндпоинта :" + endpointName);
        Endpoints endpoints = getEndpointsByName(endpointName);
        try {
            return endpoints.getSubsets().get(0).getAddresses().get(0).getIp();
        } catch (Exception e) {
            throw new EndpointException("Не удалось получать адресс хоста возкнила ошибка. " + e.getMessage());
        }
    }

    /**
     * Получить порт эндопоиинт по имени в метаданных энжопинта.
     *
     * @param endpointName - имя эндпоинта
     * @return port номер порта эндпоинта
     */
    public Integer getPort(String endpointName) {
        LOGGER.info("\nKubernetes, поиск  порта по имени эндпоинта :" + endpointName);
        Endpoints endpoints = getEndpointsByName(endpointName);
        try {
            return endpoints.getSubsets().get(0).getPorts().get(0).getPort();
        } catch (Exception e) {
            throw new EndpointException("Не удалось получать порт возкнила ошибка. " + e.getMessage());
        }
    }

}
