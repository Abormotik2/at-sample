package utils.kubernetes.ingresses;

import com.consol.citrus.kubernetes.client.KubernetesClient;
import io.fabric8.kubernetes.api.model.extensions.HTTPIngressPath;
import io.fabric8.kubernetes.api.model.extensions.Ingress;
import io.fabric8.kubernetes.api.model.extensions.IngressList;
import io.fabric8.kubernetes.api.model.extensions.IngressRule;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.vtb.at.Config;

import java.util.List;

/**
 * Вспомогательный класс для работы с ingresses kubernetes.
 * <p>
 * Помогает получать доменные имена сервисов
 */

public class IngressesHelper {
    private static Logger LOGGER = LogManager.getLogger(IngressesHelper.class);
    private KubernetesClient kubernetesClient;


    public IngressesHelper(KubernetesClient client) {
        this.kubernetesClient = client;
    }


    /**
     * Получать имя протокола
     *
     * @return String  имя протолока
     */
    public String getProtocol() {
        return kubernetesClient.getClient().getMasterUrl().getProtocol();
    }


    /**
     * Метод для получения доменного имени
     *
     * @param serviceName имя сервиса, для которого необходимо получать доменное имя
     * @return String - адрес сервиса для входа через браузер.
     */

    public String getUrl(String serviceName) {
        StringBuilder url = new StringBuilder();
        url.append(getProtocol());
        url.append("://");
        url.append(getDomainName(serviceName));
        return url.toString();
    }


    /**
     * Метод для получения доменного имени
     *
     * @param serviceName - имя сервиса, для которого необходимо получать доменное имя.
     *                    Метод может вернять null, если доменное имя по именим сервсиса не найдено.
     * @return String - домменое имя.
     */

    public String getDomainName(String serviceName) {
        IngressList ingressList = kubernetesClient.getClient().extensions().ingresses().list();
        List<Ingress> ingresses = ingressList.getItems();
        for (Ingress ingress : ingresses) {
            if (ingress.getMetadata().getName().equals(Config.getStringSystemProperty("namespace"))) {
                List<IngressRule> ingressRules = ingress.getSpec().getRules();
                for (IngressRule ingressRule : ingressRules) {
                    List<HTTPIngressPath> httpIngressPaths = ingressRule.getHttp().getPaths();
                    for (HTTPIngressPath httpIngressPath : httpIngressPaths) {
                        if (httpIngressPath.getBackend().getServiceName().equalsIgnoreCase(serviceName)) {

                            LOGGER.info("\nАдрес сервиса :'" + serviceName + "' = " + ingressRule.getHost());
                            return ingressRule.getHost();
                        }
                    }
                }
            }
        }
        LOGGER.warn("\nАдрес сервиса :'" + serviceName + "', найден");
        return null;
    }
}
