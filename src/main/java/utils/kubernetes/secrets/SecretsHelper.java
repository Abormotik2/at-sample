package utils.kubernetes.secrets;

import com.consol.citrus.kubernetes.client.KubernetesClient;
import exceptions.kubernetes.SecretsException;
import io.fabric8.kubernetes.api.model.Pod;
import io.fabric8.kubernetes.api.model.Secret;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Base64;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Вспомогательный класс для работы с secrets kubernetes.
 */

public class SecretsHelper {
    private static Logger LOGGER = LogManager.getLogger(SecretsHelper.class);
    private KubernetesClient kubernetes;


    public SecretsHelper(KubernetesClient client) {
        this.kubernetes = client;
    }

    /**
     * Поиск secrets указанных в поде
     *
     * @param pod - под из кубернетеса
     * @return Map - данных secrets
     */

    public Map<String, String> getPodsSecrets(Pod pod) {
        String secretName;
        LOGGER.info("\nKubernetes, поиск secrets для пода:" + pod.getMetadata().getName());
        try {
            secretName = pod.getSpec().getVolumes().get(0).getSecret().getSecretName();
        } catch (Exception e) {
            throw new SecretsException("При поиске secrets для пода произошла ошибка: " + e.getMessage());
        }
        Secret s = kubernetes.getClient().secrets().list().getItems().stream().
                filter(secret -> secret.getMetadata().getName().equalsIgnoreCase(secretName)).findFirst()
                .orElseThrow(() -> new SecretsException("Не найдены  secrets с именем:'" + secretName + "'"));
        return s.getData();

    }

    /**
     * Поиск secrets указанных в поде
     *
     * @param key   ключ по которому осуществляется фильтрация объектов secret.
     * @param value значение, по которому происходит поиск secret данных.
     * @return Map - данных secrets (decoded from base64)
     */
    public Map<String, String> getSecretsData(String key, String value) {
        LOGGER.info("\nKubernetes, поиск объектов secret данных c ключем " + key + ", значением " + value);
        String encodedString = encodeBase64ToString(value);
        List<Secret> secretList = getSecretsByKey(key);
        if (secretList.size() == 0) {
            throw new SecretsException("\n Не найдены объекта secret содержащие ключ " + key);
        }

        Map<String, String> encodedData = secretList.stream().
                filter(secret -> secret.getData().get(key).equalsIgnoreCase(encodedString)).findFirst()
                .orElseThrow(() -> new SecretsException("Не найден объект secret  c ключем " + key + ", значением " + value))
                .getData();
        return encodedData.entrySet().stream()
                .collect(Collectors.toMap(Map.Entry::getKey, (o) -> decodeBase64ToString(o.getValue())));
    }


    /**
     * Поиск secrets указанных в поде
     *
     * @param key - ключ по которому осуществляется фильтрация объектов secret.
     * @return List<Secret> - список объектов содержащих этот ключ.
     */
    public List<Secret> getSecretsByKey(String key) {
        LOGGER.info("\nKubernetes, поиск объектов secret данных по ключу " + key);
        return kubernetes.getClient().secrets().list().getItems().stream().
                filter(secret -> secret.getData().containsKey(key)).collect(Collectors.toList());


    }

    private String encodeBase64ToString(String value) {
        return Base64.getEncoder().encodeToString(value.getBytes());
    }

    private String decodeBase64ToString(String encodedString) {
        return new String(Base64.getDecoder().decode(encodedString));
    }


}
