package utils.kubernetes.pods;

import com.consol.citrus.kubernetes.client.KubernetesClient;
import exceptions.kubernetes.PodException;
import io.fabric8.kubernetes.api.model.Pod;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Вспомогательный класс для работы с pods kubernetes.
 */

public class PodsHelper {
    private static Logger LOGGER = LogManager.getLogger(PodsHelper.class);
    private KubernetesClient kubernetes;


    public PodsHelper(KubernetesClient client) {
        this.kubernetes = client;
    }

    /**
     * Поиск secrets указанных в поде
     *
     * @param podName - имя пода
     * @return Pod - под
     */

    public Pod getPod(String podName) {
        return kubernetes.getClient().pods().list().getItems().stream().
                filter(pod -> pod.getMetadata().getName().equalsIgnoreCase(podName)).findFirst()
                .orElseThrow(() -> new PodException("Не найден под с именем:'" + podName + "'"));

    }
}
