package utils.db;

import com.consol.citrus.context.TestContext;
import org.apache.commons.dbcp.BasicDataSource;
import ru.vtb.at.Config;
import ru.vtb.at.citrus.CitrusRunner;
import ru.vtb.at.context.Context;

import javax.sql.DataSource;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class DBUtils {
    private static CitrusRunner citrusRunner = Context.getInstance().getBean(CitrusRunner.class);
    private static TestContext testContext = citrusRunner.getTestContext();

    /**
     * Динамическое писание собственного endpoint'а, если подгруженный из бина (переданных параметров) не подходит.
     *
     * @param BDUrl    адрес
     * @param bdName   наименование БД
     * @param user     юзер бд
     * @param password пасс юзера бд
     * @return @Link DataSource
     */
    public static DataSource createBDEndpoint(String BDUrl, String bdName, String user, String password) {
        BasicDataSource dataSource = new BasicDataSource();
        dataSource.setDriverClassName(Config.getStringSystemProperty("postgresql.driver"));
        dataSource.setUrl("jdbc:postgresql://" + BDUrl + "/" + bdName);
        dataSource.setUsername(user);
        dataSource.setPassword(password);
        dataSource.setInitialSize(3);
        dataSource.setMaxActive(3);
        dataSource.setMaxIdle(2);
        return dataSource;
    }

    /**
     * Возвращает значение лежащее в переменных из TextContext.
     *
     * @param columnName наименование колонки (всегда верхний регистр), либо имя переменной
     * @return строка
     */
    public static String getResultValue(String columnName) {
        return testContext.getVariable(columnName);
    }

    /**
     * Пример метода где преобразуется в заданный тип через <u>valueOf</u>.
     *
     * @param columnName наименование переменной
     * @return boolean-результат
     */
    public static boolean getBooleanResultValue(String columnName) {
        return testContext.getVariable(columnName, boolean.class);
    }

    /**
     * Преобразование полученного результата в лист заданного типа(bool)<br>
     *  <b>OPTIONAL EXAMPLE:</b>
     *  <p>Применяется, когда в результате запроса получили >1 значения
     *  в данном случае движок цитруса склеит все значения в столбце в одну единую стрингу,<bR>
     *  где все отдельные строки будут <u>разделены символом ";"</u></p>
     * @param variableName column name or variable
     * @return converted boolean value
     */
    public static List<Boolean> getListOfBooleanFromResult(String variableName) {
        String[] abcdas = testContext.getVariable(variableName).split(";");
        //same usage w/ other types
        List<Boolean> collect = Arrays.stream(abcdas).map(Boolean::valueOf).collect(Collectors.toList());
        return collect;
    }
}
