package utils;

import org.yaml.snakeyaml.Yaml;

import java.io.InputStream;
import java.util.LinkedHashMap;
import java.util.Map;

public class YMLResourceUtils {
    public Map<String, LinkedHashMap<String, String>> readConfig(String resource) {
        InputStream input = this.getClass().getClassLoader().getResourceAsStream(resource);
        Yaml yaml = new Yaml();
        return (Map<String, LinkedHashMap<String, String>>) yaml.load(input);
    }

}
