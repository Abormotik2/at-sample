package utils.allure;

import io.qameta.allure.listener.StepLifecycleListener;
import io.qameta.allure.listener.TestLifecycleListener;
import io.qameta.allure.model.Link;
import io.qameta.allure.model.Status;
import io.qameta.allure.model.StatusDetails;
import io.qameta.allure.model.StepResult;
import io.qameta.allure.model.TestResult;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.vtb.at.Config;
import ru.vtb.at.context.Context;
import ru.vtb.at.driver.DriverManager;
import ru.vtb.at.utils.ScreenShooter;


import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Класс ответственный за добавление в Allure отчет дополнительной информации, помимо той, которая   по дефолту собирается плагином AllureCucumber4Jvm.
 */
public class AllureLogger implements StepLifecycleListener, TestLifecycleListener {

    private static final Logger LOGGER = LogManager.getLogger(AllureLogger.class.getSimpleName());
    // Переменная для сохранения скриншотов после каждого шага.
    private static final boolean stepScreenShot = Config.getBooleanSystemProperty("step.screenshot");


    @Override
    public void beforeStepStop(StepResult result) {
        if (Context.getInstance().getBean
                (DriverManager.class).isActive() & stepScreenShot) {
            AllureHelper.attachScreenShot(result.getName(), Context.getInstance().getBean
                    (ScreenShooter.class).takeScreenshot());
        }
    }


    @Override
    public void afterStepStop(StepResult result) {
        if (result.getDescription() != null && result.getDescription().contains("SoftAssert")) {
            LOGGER.info("Устанавливаем для шага: '" + result.getName() + "', статус=BROKEN");
            result.setStatus(Status.BROKEN);
            result.setDescription(result.getDescription().replace("SoftAssert:", ""));
        }
    }

    @Override
    public void beforeTestStop(TestResult result) {
        StatusDetails statusDetails = result.getStatusDetails();
        for (Link link : result.getLinks()) {
            String url = replaceVariablesInString(link.getUrl());
            link.setUrl(url);
        }

        if (result.getStatus().equals(Status.FAILED)) {
            String message = "ОШИБКА ТЕСТА:\n\"" + statusDetails.getMessage().replaceFirst("\n", "") + "\"";
            String brokenStepsMessages = findBrokenStepsMessage(result.getSteps());
            if (brokenStepsMessages != null) {
                message = message + brokenStepsMessages;
            }
            statusDetails.setMessage(message);
            result.setStatusDetails(statusDetails);
        }
    }


    /*
     * Осуществляет поиск всех шагов теста в статусе "BROKEN" и объединяет в одно информационное сообщение.
     *
     *  @param stepResults шаги теста
     *  @return String - сообщение о всех "BROKEN" шагах теста.
     */

    private String findBrokenStepsMessage(List<StepResult> stepResults) {
        ArrayList<StepResult> brokenSteps = stepResults.stream().filter(stepResult -> stepResult.getStatus().equals(Status.BROKEN)).collect(Collectors.toCollection(ArrayList::new));
        if (brokenSteps.size() > 0) {
            StringBuilder builder = new StringBuilder();
            builder.append("\n\n");
            builder.append("Другие поломки в тесте (BROKEN STEPS):");
            builder.append("\n[Также в тесте присутствуют ошибки типа SoftAssert : ");
            for (int i = 0; i < brokenSteps.size(); i++) {
                if (brokenSteps.get(i).getDescription() != null) {
                    builder.append("\n").append("Ошибка №:").append(i + 1).append("- ").append(brokenSteps.get(i).getDescription());
                } else {
                    builder.append("\n").append("Ошибка в шаге: ").append(brokenSteps.get(i).getName());
                }
            }
            builder.append("]");
            return builder.toString();
        }
        return null;
    }



    /**
     * Заменяет во входящей строке значения  ${value} на значения из конфиг файла или системной переменной.
     * Например входящий  url: https://example.com/id/${value}, будет преобразован в url вида
     *  https://example.com/id/1 ,при условии что, значение value установлено в property файле в значении 1 .
     *  Или было установлено при запуске командной -DValu="1".
     *
     *  @param str строка в которой необходимо заменить переменные вида  ${value}
     *  @return String - Строка после замены переменных на конечные значения
     */
    private String replaceVariablesInString(String str) {
        StringBuilder newStr = new StringBuilder();
        final String PREFIX = "${";
        final String SUFFIX = "}";

        boolean isVarComplete;
        StringBuffer variableNameBuf = new StringBuffer();

        int startIndex = 0;
        int curIndex;
        int searchIndex;

        while ((searchIndex = str.indexOf(PREFIX, startIndex)) != -1) {
            int control = 0;
            isVarComplete = false;

            curIndex = searchIndex + PREFIX.length();

            while (curIndex < str.length() && !isVarComplete) {
                if (str.indexOf(PREFIX, curIndex) == curIndex) {
                    control++;
                }

                if ((!Character.isJavaIdentifierPart(str.charAt(curIndex)) && (str.charAt(curIndex) == SUFFIX.charAt(0))) || (curIndex + 1 == str.length())) {
                    if (control == 0) {
                        isVarComplete = true;
                    } else {
                        control--;
                    }
                }

                if (!isVarComplete) {
                    variableNameBuf.append(str.charAt(curIndex));
                }
                ++curIndex;
            }

            final String value = Config.getStringSystemProperty(variableNameBuf.toString());
            if (value == null) {
                LOGGER.warn("Не найдено значение переменной:'" + variableNameBuf.toString() + "'."
                        + "В ссылку:'" + str + "', вместо переменной '" + variableNameBuf + "',"
                        + "будет подставлен null");
            }
            newStr.append(str, startIndex, searchIndex);
            newStr.append(value);
            startIndex = curIndex;
            variableNameBuf = new StringBuffer();
        }
        newStr.append(str.substring(startIndex));

        return newStr.toString();
    }


}