package utils.allure;

import io.qameta.allure.util.PropertiesUtils;
import ru.vtb.at.Config;
import ru.vtb.at.exceptions.FrameworkRuntimeException;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

/**
 * Вспомогательный класс, для добавление в блок Environment в отчете Allure необходимых значений.
 */

public class AllureEnvironmentSaver {
    private  static Properties env = new Properties();

    /**
     * Добавление данных в блок  Environment для отчета Allure
     *
     *  @param key - название переменной
     *  @param value - значение переменной
     */
    public static void addEnvVariable(String key,String value){
        env.setProperty(key,value);
    }



    /**
     * Сохранение переменных  в отчет Allure
     */
    public static void save() {
        env.setProperty("Browser", getProperty("browser"));
        env.setProperty("Stand Url", getProperty("site.url"));
        env.setProperty("Namespace", getProperty("namespace"));
        env.setProperty("Kuber Url", getProperty("kuber.url"));

        String path = PropertiesUtils.loadAllureProperties().getProperty("allure.results.directory");
        try {
            Path allureResultsDirectory = Files.createDirectories(Paths.get(path));
            try (OutputStream os = Files.newOutputStream(allureResultsDirectory.resolve("environment.properties"))) {
                env.store(os, "Allure environment");
            }
        } catch (IOException e) {
            throw new FrameworkRuntimeException("Ошибка сохранения Allure environment", e);
        }
    }

    /**
     * Вспомогательный метод, для считывания значений переменных из property файла(конфига) или системных переменных
     *
     *  @param value - название переменной
     * @return String - значение переменной
     */
    private static String getProperty(String value) {
        return String.valueOf(System.getProperty(value, Config.getStringSystemProperty(value)));
    }
}
