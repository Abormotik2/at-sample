package utils.database;

import utils.kubernetes.Kubernetes;

import java.util.Map;


/**
 * Вспомогателный класс для работы с БД
 */
public class DataBaseHelper {


    /**
     * Получить пароль и логин для БД
     *
     * @param schemes - схема к которой нужно подключение
     * @return map c логином и паролем
     */
    public static Map<String, String> getUserInfo(Schemes schemes) {
        return Kubernetes.secretsHeplper.getSecretsData("username", schemes.getValue() + "_ms");

    }


}
