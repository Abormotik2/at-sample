package utils.database;

import ru.vtb.at.exceptions.FrameworkRuntimeException;

import java.util.Arrays;

public enum Schemes {
    RRKO("rrko"),
    NTN("ntn"),
    AC("ac"),
    ACCOUNT("account"),
    AGREEMENT_TYPE("agreementtype"),
    BRN("brn"),
    BANK("bank"),
    CCY("ccy"),
    CLN("cln"),
    CNT("cnt"),
    CNTR("cntr"),
    CR("cr"),
    ISSUECERT("issuecert"),
    LTRS("ltrs"),
    OPENACC("openacc"),
    PMNTSREF("pmntsref"),
    STMT("stmt"),
    EUS("eus"),
    EXTSYS("extsys"),
    MINBANK("minbank"),
    WBANK("wbank"),
    VKTR("vktr"),
    EC_CRA("ec_cra"),
    EC_BCMA("ec_bcma"),
    SCS("scs"),
    TRANSIT("taw"),
    MESSAGES("ntn");

    String value;

    Schemes(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static Schemes getByValue(String requiredValue) {
        return Arrays.stream(Schemes.values()).filter(schemes -> schemes.getValue().equalsIgnoreCase(requiredValue))
                .findFirst().orElseThrow(() -> new FrameworkRuntimeException("Не найдена схема со значением :" + requiredValue));
    }
}
