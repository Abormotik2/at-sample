package utils.business;


import deprecated.api.services.business.test.*;
import ru.vtb.at.Config;

/*
 * todo удалить или починить неиспользуемый класс VTBDBOAUTO-232
 */
@Deprecated
public class EntityHelper {
    private final String className;
    private final Object classInstance;

    public String getClassName() {
        return className;
    }

    public Object getClassInstance() {
        return classInstance;
    }

    EntityHelper(String className, Object classInstance) {
        this.className = className;
        this.classInstance = classInstance;
    }

    public static Class<?> getInstanceOfClass(String caption) {
//        List<ClassLoader> classLoaderList = new LinkedList<ClassLoader>();
//        classLoaderList.add(ClasspathHelper.contextClassLoader());
//        classLoaderList.add(ClasspathHelper.staticClassLoader());
//
//        Reflections reflections = new Reflections(new ConfigurationBuilder()
//                .setScanners(new SubTypesScanner(false), new ResourcesScanner(), new TypeAnnotationsScanner())
//                .setUrls(ClasspathHelper.forClassLoader(classLoaderList.toArray(new ClassLoader[0])))
//                .filterInputsBy(new FilterBuilder().include(FilterBuilder.prefix("deprecated.api.services.business.test"))));
//        return reflections.getTypesAnnotatedWith(DPData.class)
//                .stream()
//                .filter(aClass -> aClass.getAnnotation(DPData.class).caption().equals(caption))
//                .filter(EntityHelper::ieStarted)
//                .findAny().orElseThrow(FrameworkRuntimeException::new);
        return null;
    }


    private static boolean ieStarted(Class<?> aClass) {
        if (aClass.equals(CertTemplateDPCommon.class) | aClass.equals(CertTemplateDPIE.class)) {
            return !(aClass.equals(CertTemplateDPCommon.class) & Config.getStringSystemProperty("browser").equalsIgnoreCase("internet explorer") |
                    aClass.equals(CertTemplateDPIE.class) & !Config.getStringSystemProperty("browser").equalsIgnoreCase("internet explorer"));
        }
        return true;
    }
}
