package utils;

import java.util.HashMap;

public class Stash {


     private static Stash stashInstance = null;

    private Stash(){ }
    public static Stash getStashInstance(){
        if(stashInstance==null){
            stashInstance= new Stash();
        }
        return stashInstance;
    }

    private static HashMap<String,Object> stashMap = new HashMap<>();

    public void addStashObj(String key, Object stashObj){
        stashMap.put(key,stashObj);
    }

    public Object getStash(String key){
        return stashMap.getOrDefault(key, null);
    }

    public Object getStashIfExist(String stashKey){
        return stashMap.getOrDefault(stashKey, stashKey);
    }
}
