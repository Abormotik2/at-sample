package utils;

import com.consol.citrus.dsl.endpoint.CitrusEndpoints;
import com.consol.citrus.dsl.runner.DefaultTestRunner;
import com.consol.citrus.http.client.HttpClient;
import com.consol.citrus.message.Message;
import com.consol.citrus.message.MessageType;
import org.apache.http.entity.ContentType;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpMethod;
import ru.vtb.at.Config;
import ru.vtb.at.utils.api.ApiUtils;
import ru.vtb.at.utils.api.citrus.CitrusRequestBuilder;
import ru.vtb.at.utils.api.helpers.Helpers;
import ru.vtb.at.utils.api.helpers.YMLResourceUtils;
import ru.vtb.at.utils.api.kubernetes.Kubernetes;
import ru.vtb.at.citrus.CitrusRunner;
import ru.vtb.at.context.Context;


import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class ApiClient {
    private ApplicationContext context;
    private DefaultTestRunner citrusRunner;
    private String configPath = "microservice.config";
    private Map requestsInfo;
    private ApiUtils apiUtils;


    public ApiClient() {
        /*Необходимо передать путь до yam-словаря запросов*/
        apiUtils = new ApiUtils(configPath);
        context = Context.getInstance();
        this.citrusRunner = this.context.getBean(CitrusRunner.class);
        requestsInfo = new YMLResourceUtils().readConfig(ru.vtb.at.datavalidator.Config.getStringSystemProperty(configPath));
    }

    /**
     * Метод вощвращает JSONObject из сервиса
     *
     * @param requestName = имя запроса в словаре yml
     * @param map         = параметры для формирвоания эндпоинта
     * @param json        = тело запроса
     * @return
     */
    @Deprecated
    public JSONObject getDataFromService(String requestName, Map<String, String> map, String json) {
        return apiUtils.getDataFromService(requestName, map, json, getToken());
    }

    /**
     * Метод вощвращает JSONObject из сервиса
     *
     * @param requestName = имя запроса в словаре yml
     * @param map         = параметры для формирвоания эндпоинта
     * @param json        = тело запроса
     * @param token       = токен клиента
     * @return
     */
    @Deprecated
    public JSONObject getDataFromService(String requestName, Map<String, String> map, String json, String token) {
        return apiUtils.getDataFromService(requestName, map, json, token);
    }

    /**
     * @param requestName
     * @param map
     * @return
     */
    @Deprecated
    public JSONObject getDataFromService(String requestName, HashMap<String, String> map) {
        return getDataFromService(requestName, map, null);
    }

    @Deprecated
    public <T> JSONArray getDataListFromService(Class<T> entity, String requestName, HashMap<String, String> parameters) {
        return apiUtils.getDataListFromService(requestName, parameters, null, getToken());
    }

    /**
     * Метод возвращает токен для авторизации в ДБО.
     * На данный момент токен захардкожен в файл конфига
     *
     * @return
     */
    @Deprecated
    public String getToken() {
        return Config.getStringSystemProperty("admin.token");
    }

    @Deprecated
    public String getDataStringFromService(String requestName, HashMap<String, String> parameters, String payload) {
        return apiUtils.getDataStringFromService(requestName, parameters, payload, getToken());
    }

    @Deprecated
    public String getDataStringFromService(String requestName, Map<String, String> parameters, String payload, String token) {
        return apiUtils.getDataStringFromService(requestName, parameters, payload, token);
    }

    /**
     * Метод возвращает клиентский токен
     *
     * @return
     */
    public String getClientToken() {
        return apiUtils.getClientToken();
    }

//    /**
//     * Метод возвращает токен активной сессии
//     * @param login - логин пользователя
//     * @param password - пароль пользователя
//     *
//     * @return
//     */
//    public String getUserAuthTokenByLoginPassword(String login, String password) {
//        return apiUtils.getUserAuthTokenByLoginPassword(login, password);
//    }

    public Message getDataStringFromService(String requestName, Map<String, String> headers, Map<String,
            String> parameters, Map<String, String> queryParams, String payload, String token) {
        if (token != null && !token.isEmpty()) {
            headers.put("Authorization", "Bearer " + token);
        }

        return (new CitrusRequestBuilder(this.citrusRunner))
                .sendCitrusRequest(
                        this.getEndpoint(requestName),
                        getRequestType(requestName),
                        Helpers.replaceVariables(parameters, getRequest(requestName, this.configPath)),
                        queryParams,
                        headers,
                        ContentType.APPLICATION_JSON.toString(),
                        ContentType.APPLICATION_JSON.getMimeType(),
                        payload == null ? "" : payload,
                        MessageType.JSON);
    }

    private HttpClient getEndpoint(String caption) {
        return this.getEndpointByMServiceName((String) ((LinkedHashMap) this.requestsInfo.get(caption)).get("serviceName"));
    }

    private HttpClient getEndpointByMServiceName(String mServiceName) {
        return CitrusEndpoints.http().client().requestUrl(Kubernetes.ingresses.getUrl(mServiceName)).build();
    }

    public HttpMethod getRequestType(String caption) {
        return HttpMethod.valueOf(String.valueOf(((LinkedHashMap) this.requestsInfo.get(caption)).get("requestType")));
    }

    public String getRequest(String caption, String config) {
        return String.valueOf(((LinkedHashMap) this.requestsInfo.get(caption)).get("request"));
    }
}
