package pages.vtb.pages.ru.blocks;

import io.qameta.atlas.webdriver.extension.FindBy;
import pages.html_elements.Button;
import pages.html_elements.InputField;
import pages.html_elements.Text;
import ru.vtb.at.pages.block.AbstractBlockElement;
import ru.yandex.qatools.matchers.webdriver.EnabledMatcher;


public interface Блок_авторизации extends AbstractBlockElement,
        InputField.WithInput, Button.WithButton, Text.WithText {

    default void ввести_логин(String name) {
        input("Введите логин").sendKeys(name);
    }

    default void ввести_пароль(String password) {
        input("Введите пароль").sendKeys(password);
    }

    default void нажать_кнопку_найти() {
        button("Найти").click();
    }


    default void авторизоваться(String логин, String пароль) {
        input("Введите логин").sendKeys(логин);
        input("Введите пароль").sendKeys(пароль);
        button("Войти").click();
    }

    default void authByLogin(String login, String password) {
        if (!text("Вход по сертификату").isDisplayed()) {
            text("Вход по логину").click();
        }
        input("Введите логин").waitUntil(EnabledMatcher.enabled(), 10).sendKeys(login);
        input("Введите пароль").waitUntil(EnabledMatcher.enabled(), 10).sendKeys(password);
        button("Войти").click();
    }


    interface С_блоком_авторизации extends AbstractBlockElement {
        @FindBy("//*[contains(@class, 'login-body__form')]")
        Блок_авторизации блок_авторизации();
    }
}