package pages.vtb.pages.ru;

import pages.html_elements.Text;
import pages.vtb.pages.ru.blocks.Блок_авторизации;
import ru.vtb.at.pages.AbstractPage;
import ru.vtb.at.pages.annotations.Title;

@Title("ВТБ Бизнес-онлайн")
public interface Страница_авторизации extends AbstractPage,
        Блок_авторизации.С_блоком_авторизации, Text.WithText {
}