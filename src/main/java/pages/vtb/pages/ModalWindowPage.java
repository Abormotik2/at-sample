package pages.vtb.pages;

import io.qameta.atlas.webdriver.ElementsCollection;
import io.qameta.atlas.webdriver.extension.FindBy;
import pages.html_elements.*;
import pages.vtb.pages.blocks.FormBlockElement;
import ru.vtb.at.pages.AbstractPage;
import ru.vtb.at.pages.annotations.Title;
import ru.vtb.at.pages.block.AbstractBlockElement;
import ru.vtb.at.pages.element.UIElement;

@Title(value = "Модальное окно")
public interface ModalWindowPage extends AbstractPage, AbstractBlockElement, Button.WithButton, Text.WithText, InputField.WithInput, FormBlockElement, Row.WithRows, DropDown.WithDropDown, PopupWindow.WithPopupWindow {

    @FindBy("//div[contains(@class,'modal-form__sidebar-content')]")
    ModalWindowPage modalFormSidebar();

    @FindBy("//div[contains(@class,'modal-form__header')]")
    ModalWindowPage modalFormHeader();

    @FindBy("(//div[contains(@class,'modal-form__content')])[last()]")
    ModalWindowPage modalFormContent();

    @FindBy("//div[starts-with(@class,'modal-layout__title')]")
    ModalWindowPage popUpTitle();

    @FindBy("//span[starts-with(@class,'modal-layout__close')]")
    ModalWindowPage closePopUpTitle();

    @FindBy(".//div[contains(@class,'tabs__scrollbar')]/div")
    ElementsCollection<UIElement> tabsList();

    interface WithModalWindow extends FormBlockElement {

        @FindBy("(//div[contains(@class,'modals__window')])[last()]")
        ModalWindowPage modalWindow();
    }
}
