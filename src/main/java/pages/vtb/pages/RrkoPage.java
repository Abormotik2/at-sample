package pages.vtb.pages;

import pages.vtb.pages.blocks.FilterElement;
import pages.vtb.pages.blocks.SidebarBlock;
import pages.vtb.pages.blocks.TableElement;
import ru.vtb.at.pages.AbstractPage;
import ru.vtb.at.pages.annotations.Title;

@Title(value = "Платёжные поручения")
public interface RrkoPage extends AbstractPage, FilterElement, TableElement, SidebarBlock.WithSidebarMenuBlock {

}
