package pages.vtb.pages.blocks;

import io.qameta.atlas.webdriver.extension.FindBy;
import pages.html_elements.Button;
import pages.html_elements.Text;
import ru.vtb.at.pages.annotations.Title;
import ru.vtb.at.pages.block.AbstractBlockElement;

@Title(value = "Заголовок главной страницы")
public interface HeaderBlock extends AbstractBlockElement,
             Button.WithButton, Text.WithText {

    interface WithHeaderBlock extends AbstractBlockElement {
        @FindBy("//div[@class='headline-MG0']")
        HeaderBlock headerBlock();
    }
}