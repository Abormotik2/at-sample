package pages.vtb.pages.blocks;

import io.qameta.atlas.webdriver.extension.FindBy;
import pages.html_elements.Button;
import pages.html_elements.Text;
import ru.vtb.at.pages.block.AbstractBlockElement;

public interface CreateDocBlock extends AbstractBlockElement, Button.WithButton, Text.WithText{

    interface WithCreateDocBlock extends AbstractBlockElement{
        @FindBy("//div[@class='create-doc-container-2Mn']")
        CreateDocBlock createDocBlock();
    }
}
