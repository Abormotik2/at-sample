package pages.vtb.pages.blocks;

import extensions.Is;
import io.qameta.atlas.webdriver.extension.FindBy;
import pages.html_elements.*;
import ru.vtb.at.exceptions.FrameworkRuntimeException;
import ru.vtb.at.pages.block.AbstractBlockElement;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

import static com.jayway.jsonpath.Filter.filter;

public interface FormBlockElement extends AbstractBlockElement, Row.WithRows, SubRow.WithSubRows, DropDown.WithDropDown, Button.WithButton {

    String XPATH = "//div[contains(@class, 'grid-3Nc grid--twelve-1Ly')][.//div[contains(@class, 'text-1Ot')]]";

    @FindBy(".//div[contains(@class, 'text-1Ot')]")
    Block name();

    @FindBy(XPATH)
    List<FormBlockElement> childBlocks();

    default Row getRow(String name) {
        return rows().stream()
                .filter(Row::isDisplayed)
                .filter(row -> row.getName().replace("\n", "").equalsIgnoreCase(name) | row.getName().replace("\n", " ").equalsIgnoreCase(name))
                .findFirst()
                .orElseThrow(() -> new FrameworkRuntimeException("В блоке отсутствует запрашиваемое поле"));
    }

    default List<SubRow> getSubRowsByName(String fieldName) {
        return subrows().stream().filter(t -> t.getName().equalsIgnoreCase(fieldName)).collect(Collectors.toList());
    }


    default SubRow getSubRows(String name) {
        return subrows().stream()
                .filter(SubRow::isDisplayed)
                .filter(subRow -> subRow.getName().replace("\n", "").equalsIgnoreCase(name) | subRow.getName().replace("\n", " ").equalsIgnoreCase(name))
                .findFirst()
                .orElseThrow(()-> new FrameworkRuntimeException("В блоке отсутствует запрашиваемое поле"));
    }

    default SubRow getSubRowsTextArea(String name) {
        return subrows().stream()
                .filter(SubRow::isDisplayed)
                .filter(subRow -> subRow.getTextAreaName().replace("\n", "").equalsIgnoreCase(name) | subRow.getTextAreaName().replace("\n", " ").equalsIgnoreCase(name))
                .findFirst()
                .orElseThrow(()-> new FrameworkRuntimeException("В блоке отсутствует запрашиваемое поле"));
    }

    default String getName() {
        try {
            if (this.name().getText().contains("\n")) {
                return this.name().getText().replace("\n", " ").trim();
            } else return this.name().getText().replace("*", " ").trim();
        } catch (NoSuchElementException nse) {
            return " ";
        }
    }


    default FormBlockElement getChildBlock(String childBlockName) {
        return childBlocks().stream()
                .filter(Is::visible)
                .filter(block -> block.getName().equalsIgnoreCase(childBlockName))
                .findFirst()
                .orElseThrow(() -> new FrameworkRuntimeException("Отсутствует блок " + childBlockName));
    }
}
