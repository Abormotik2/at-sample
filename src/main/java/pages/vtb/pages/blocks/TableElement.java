package pages.vtb.pages.blocks;


import io.qameta.atlas.webdriver.extension.FindBy;
import pages.html_elements.Button;
import pages.html_elements.CustomCheckBox;
import pages.html_elements.Row;
import ru.vtb.at.exceptions.FrameworkRuntimeException;
import ru.vtb.at.pages.block.AbstractBlockElement;
import utils.Stash;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public interface TableElement extends AbstractBlockElement, Button.WithButton, Row.WithRows, CustomCheckBox.WithCheckBox {

    @FindBy("//div[contains(@class,'table__rows')]//div[@role='rowgroup']/div")
    List<Row> viewRows();

    @FindBy("checkbox-filter")
    CustomCheckBox checkBox();

    default Row getRow(String name) {
        return viewRows().stream()
                .filter(Row::isDisplayed)
                .filter(row -> row.getText().contains(Stash.getStashInstance().getStashIfExist(name).toString()))
                .findFirst()
                .orElseThrow(() -> new FrameworkRuntimeException("В блоке отсутствует запрашиваемое поле"));
    }

    default List<Row> getRows(List<String> names) {
        return viewRows().stream()
                .filter(Row::isDisplayed)
                .filter(row -> names.contains(row.getText())).collect(Collectors.toList());
    }

    default boolean hasRow(String name) {
        return !getRows(Collections.singletonList(Stash.getStashInstance().getStash(name).toString())).isEmpty();
    }

    default Row getRandomRow() {
        List<Row> finalRows = viewRows().stream().filter(Row::isDisplayed).filter(row -> row.getNumber().length() > 0).collect(Collectors.toList());
        Row randomRow = finalRows.get((int) (Math.random() * finalRows.size()));
        Stash.getStashInstance().addStashObj("номер документа", randomRow.getNumber());
        return randomRow;
    }

    default void checkRow(Row row) {
        row.click();
        row.customCheckBox().setChecked(true);
    }

    default void unCheckRow(Row row) {
        row.customCheckBox().setChecked(false);
    }
}
