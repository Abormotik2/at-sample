package pages.vtb.pages.blocks;

import extensions.Is;
import io.qameta.atlas.webdriver.extension.FindBy;
import pages.html_elements.Button;
import pages.html_elements.Text;
import ru.vtb.at.exceptions.FrameworkRuntimeException;
import ru.vtb.at.pages.block.AbstractBlockElement;

import java.util.List;

public interface FilterElement extends AbstractBlockElement, Button.WithButton, Text.WithText {

    @FindBy("//div[contains(@class,'scroller__filter')]")
    FilterElement filterLine();

    @FindBy(".//div[contains(@class,'tabs__scrollbar')]/div")
    List<FilterElement> tabsList();

    @FindBy(".//div[contains(@class,'filter__header')]")
    FilterElement filter();

    default FilterElement getTab(String tabName){
        return tabsList().stream()
                .filter(Is::visible)
                .filter(block -> block.getText().contains(tabName))
                .findFirst()
                .orElseThrow(() -> new FrameworkRuntimeException("Отсутствует блок " + tabName));
    }

}
