package pages.vtb.pages.blocks;

import io.qameta.atlas.webdriver.extension.FindBy;
import pages.html_elements.Button;
import pages.html_elements.CustomCheckBox;
import pages.html_elements.Text;
import ru.vtb.at.pages.block.AbstractBlockElement;

public interface ActionElement extends AbstractBlockElement, Button.WithButton, Text.WithText, CustomCheckBox.WithCheckBox {

    @FindBy("//div[contains(@class,'scroller__action-bar')]")
    ActionElement actionLine();
}
