package pages.vtb.pages.blocks;

import io.qameta.atlas.webdriver.extension.FindBy;
import pages.html_elements.Button;
import pages.html_elements.Text;
import ru.vtb.at.pages.annotations.Title;
import ru.vtb.at.pages.block.AbstractBlockElement;
import ru.vtb.at.pages.element.UIElement;

public interface CreateMenuBlock extends AbstractBlockElement, Button.WithButton, Text.WithText {

    @FindBy("//div[starts-with(@class,'modal-form__header')]")
    HeaderBlock header();

        interface WithCreateMenuBlock extends AbstractBlockElement {
            @FindBy("//div[@class='create-doc-container-2Mn']")
            CreateMenuBlock createMenuBlock();
        }
    }
