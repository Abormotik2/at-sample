package pages.vtb.pages.blocks;

import io.qameta.atlas.webdriver.extension.FindBy;
import pages.html_elements.Button;
import pages.html_elements.Text;
import ru.vtb.at.pages.AbstractPage;
import ru.vtb.at.pages.annotations.Title;
import ru.vtb.at.pages.block.AbstractBlockElement;

@Title(value = "Заголовок главной страницы")
public interface SidebarBlock extends AbstractPage,AbstractBlockElement, Button.WithButton, Text.WithText {

    interface WithSidebarMenuBlock extends AbstractBlockElement {
        @FindBy("//aside[contains(@class,'layout__sidebar')]")
        SidebarBlock sidebarBlock();
    }
}
