package pages.vtb.pages.blocks;

import io.qameta.atlas.webdriver.extension.FindBy;
import pages.html_elements.Button;
import pages.html_elements.InputField;
import pages.html_elements.Text;
import ru.vtb.at.pages.annotations.Title;
import ru.vtb.at.pages.block.AbstractBlockElement;
import ru.yandex.qatools.matchers.webdriver.EnabledMatcher;

import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.hasSize;

@Title(value = "Блок авторизации")
public interface AuthorizationBlock extends AbstractBlockElement,
        InputField.WithInput, Button.WithButton, Text.WithText {


    interface WithAuthorizationBlock extends AbstractBlockElement {
        @FindBy("//*[contains(@class, 'login-body__form')]")
        AuthorizationBlock authorizationBlock();
    }
}
