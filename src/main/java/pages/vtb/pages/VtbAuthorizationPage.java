package pages.vtb.pages;

import io.qameta.atlas.webdriver.ElementsCollection;
import io.qameta.atlas.webdriver.extension.FindBy;
import io.qameta.atlas.webdriver.extension.Name;
import pages.html_elements.Text;
import pages.vtb.pages.blocks.AuthorizationBlock;
import ru.vtb.at.pages.AbstractPage;
import ru.vtb.at.pages.annotations.Title;
import ru.vtb.at.pages.element.UIElement;
import ru.yandex.qatools.matchers.webdriver.EnabledMatcher;

import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.hasSize;

@Title(value = "Страница авторизации")
public interface VtbAuthorizationPage extends AbstractPage,
        AuthorizationBlock.WithAuthorizationBlock, Text.WithText {

    default public void authByCertificate(String userName, String password) {
        if (!authorizationBlock().text("Вход по логину").isDisplayed()) {
            authorizationBlock().text("Вход по сертификату").click();
        }
        if (!authorizationBlock().text(userName).isDisplayed()) {
            authorizationBlock().text("Выберите сертификат").click();
            this.certs().filter(uiElement -> uiElement.getText().contains(userName)).should("Отсутствует сертификат в списке  выбора ", hasSize(greaterThan(0))).get(0).click();
        }
        authorizationBlock().input("Введите пароль").waitUntil(EnabledMatcher.enabled(), 10).sendKeys(password);
        authorizationBlock().button("Войти").click();
    }

    default void authByLogin(String login, String password) {
        if (!authorizationBlock().text("Вход по сертификату").isDisplayed()) {
            authorizationBlock().text("Вход по логину").click();
        }
        authorizationBlock().input("Введите логин").waitUntil(EnabledMatcher.enabled(), 10).sendKeys(login);
        authorizationBlock().input("Введите пароль").waitUntil(EnabledMatcher.enabled(), 10).sendKeys(password);
        authorizationBlock().button("Войти").click();
    }


    @Name("certs")
    @FindBy("//li[@role='menuitem']")
    ElementsCollection<UIElement> certs();

}






