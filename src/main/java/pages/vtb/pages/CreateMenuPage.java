package pages.vtb.pages;

import pages.vtb.pages.blocks.CreateMenuBlock;
import pages.vtb.pages.blocks.HeaderBlock;
import ru.vtb.at.pages.AbstractPage;
import ru.vtb.at.pages.annotations.Title;

@Title(value = "Создать")
public interface CreateMenuPage extends CommonPage, CreateMenuBlock,CreateMenuBlock.WithCreateMenuBlock {

    @Override
    default HeaderBlock getHeader(){
        return header();
    }
}
