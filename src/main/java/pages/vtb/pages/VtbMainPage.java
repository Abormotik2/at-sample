package pages.vtb.pages;

import pages.html_elements.Button;
import pages.vtb.pages.blocks.ActionElement;
import pages.vtb.pages.blocks.CreateMenuBlock;
import pages.vtb.pages.blocks.HeaderBlock;
import pages.vtb.pages.blocks.SidebarBlock;
import pages.vtb.pages.commons_blocks.UserInfoBlock;
import ru.vtb.at.pages.AbstractPage;
import ru.vtb.at.pages.annotations.Title;

@Title(value = "Главная страница")
public interface VtbMainPage extends AbstractPage, UserInfoBlock.WithUserInfoBlock, HeaderBlock.WithHeaderBlock, CreateMenuBlock.WithCreateMenuBlock, SidebarBlock.WithSidebarMenuBlock, Button.WithButton, ActionElement {
}