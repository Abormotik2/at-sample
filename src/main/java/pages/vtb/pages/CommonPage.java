package pages.vtb.pages;

import pages.vtb.pages.blocks.HeaderBlock;
import ru.vtb.at.pages.AbstractPage;
import ru.vtb.at.pages.element.UIElement;

public interface CommonPage extends AbstractPage, UIElement {

    HeaderBlock getHeader();
}
