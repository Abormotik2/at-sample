package pages.vtb.pages;

import io.qameta.atlas.webdriver.extension.FindBy;
import pages.html_elements.Button;
import pages.html_elements.Text;
import pages.vtb.pages.blocks.HeaderBlock;
import ru.vtb.at.pages.annotations.Title;
import ru.vtb.at.pages.element.UIElement;

@Title("Импорт документов. Загрузка файлов")
public interface ImportPage extends CommonPage, UIElement, Text.WithText, Button.WithButton {

    @FindBy("//div[starts-with(@class,'modal-form__header')]")
    HeaderBlock header();

    @FindBy("//*[contains(@type,'file')]")
    UIElement uploadField();

    @Override
    default HeaderBlock getHeader(){
        return header();
    }
}
