package pages.vtb.pages.commons_blocks;

import io.qameta.atlas.webdriver.extension.FindBy;
import pages.html_elements.Button;
import pages.vtb.pages.ModalWindowPage;
import ru.vtb.at.pages.annotations.Title;
import ru.vtb.at.pages.block.AbstractBlockElement;

@Title(value = "Блок информации о пользователе")
public interface UserInfoBlock extends AbstractBlockElement, ModalWindowPage.WithModalWindow {

    @FindBy(".//*[contains(@class,'logout')]")
    Button logOutButton();

    default void logOut() {
        logOutButton().click();
        modalWindow().button("Выйти").click();
    }


    interface WithUserInfoBlock extends AbstractBlockElement {
        @FindBy(".//*[contains(@class,'userInfoBar')]")
        UserInfoBlock userInfoBlock();
    }
}
