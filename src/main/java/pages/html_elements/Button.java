package pages.html_elements;

import io.qameta.atlas.webdriver.extension.FindBy;
import io.qameta.atlas.webdriver.extension.Param;
import pages.vtb.pages.ModalWindowPage;
import ru.vtb.at.pages.annotations.Title;
import ru.vtb.at.pages.element.UIElement;


@Title(value = "Кнопка")
public interface Button extends UIElement, Text.WithText {


    interface WithButton extends UIElement {
        @FindBy(".//button")
        Button button();

        @FindBy(".//text()[normalize-space(.) ='{{ name }}']/ancestor::*[self::button][1]|.//*[contains(@class,'button')and contains(text(),'{{ name }}')]")
        Button button(@Param("name") String name);

        @FindBy(".//span[contains(@class, 'masked-input__button')]")
        Button maskedButton();
    }
}