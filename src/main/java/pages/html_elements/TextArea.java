package pages.html_elements;

import io.qameta.atlas.webdriver.extension.FindBy;
import ru.vtb.at.pages.annotations.Title;
import ru.vtb.at.pages.element.UIElement;

@Title(value = "Текстовое поле")
public interface TextArea extends UIElement {


    interface WithTextArea extends UIElement {
        @FindBy(".//textarea")
        TextArea text();
    }
}

