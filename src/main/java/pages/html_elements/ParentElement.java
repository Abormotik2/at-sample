package pages.html_elements;

import io.qameta.atlas.webdriver.extension.FindBy;
import io.qameta.atlas.webdriver.extension.Param;
import ru.vtb.at.pages.element.UIElement;


/**
 * Родительский элемент для всех наследников
 */

public interface ParentElement extends UIElement, Text.WithText {


    public interface WithButton extends UIElement {
        @FindBy(".//button")
        ParentElement button();

        @FindBy(".//text()[normalize-space(.) ='{{ name }}']/ancestor::*[self::button][1]|.//*[contains(@class,'button')and contains(text(),'{{ name }}')]")
        ParentElement button(@Param("name") String name);
    }
}
