package pages.html_elements;

import extensions.Is;
import io.qameta.atlas.webdriver.AtlasWebElement;
import io.qameta.atlas.webdriver.ElementsCollection;
import io.qameta.atlas.webdriver.extension.FindBy;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import ru.vtb.at.context.Context;
import ru.vtb.at.driver.DriverManager;
import ru.vtb.at.exceptions.FrameworkRuntimeException;
import ru.vtb.at.pages.block.AbstractBlockElement;
import ru.vtb.at.pages.element.UIElement;
import utils.Wait;

import java.util.Optional;

public interface Row extends AbstractBlockElement, InputField.WithInput, Button.WithButton, CheckBox.WithCheckBox, CustomCheckBox.WithCheckBox, UIElement {

    @FindBy(".//label | .//label/span")
    AtlasWebElement nameElement();

    default InputField getInput(int inputNumberFrom_1) {
        if (inputNumberFrom_1 > inputs().size() || inputNumberFrom_1 < 1) {
            throw new FrameworkRuntimeException();
        }
        return inputs().get(inputNumberFrom_1 - 1);
    }

    default InputField getInput() {
        return getInput(1);

    }

    default void doubleClickRow(){
        Actions action = new Actions(Context.getInstance().getBean(DriverManager.class).getDriver());
        action.doubleClick(this).perform();
    }

    default String getName() {
        if (nameElement().isDisplayed()) {
            if (Is.visible(nameElement())) {
                return nameElement().getText().replace("*", "").replace("\n", "").trim();
            } else {
                return this.getText().replace("*", "").replace("\n", "").trim();
            }
        } else {
            return "";
        }
    }

    default Row getFirst(){
        return (Row)this.findElement(By.xpath("(.//div[@role='rowgroup']/div)[1]"));
    }

    default String getNumber() {
        return this.findElement(By.xpath(".//div[starts-with(@class,'cell-group')][last()]/div[count(//span[text()='№']/ancestor::*[contains(@class,'table__cell')])-1]")).getText();
    }

    default String getStatus() {
        return this.findElement(By.xpath(".//div[starts-with(@class,'cell-group')][last()]/div[count(//span[text()='Статус']/ancestor::*[contains(@class,'table__cell')])-1]")).getText();
    }

    interface WithRows extends AbstractBlockElement {
        @FindBy(".//div[contains(@class, 'row')]")
        ElementsCollection<Row> rows();
    }
}




