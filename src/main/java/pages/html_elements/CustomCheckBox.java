package pages.html_elements;

import io.qameta.atlas.webdriver.extension.FindBy;
import org.openqa.selenium.By;
import ru.vtb.at.pages.element.UIElement;

public interface CustomCheckBox extends CheckBox {

    @Override
    default boolean isChecked() {
        return this.findElement(By.xpath("./input")).isSelected();
    }

    public interface WithCheckBox extends UIElement {
        @FindBy(".//label[starts-with(@class, 'checkbox') and input and span]")
        CustomCheckBox customCheckBox();
    }
}
