package pages.html_elements;

import io.qameta.atlas.webdriver.AtlasWebElement;
import io.qameta.atlas.webdriver.extension.FindBy;
import io.qameta.atlas.webdriver.extension.Param;
import org.openqa.selenium.By;
import ru.vtb.at.pages.block.AbstractBlockElement;

import java.util.Optional;

public interface PopupWindow extends AbstractBlockElement, Row.WithRows, InputField.WithInput, Button.WithButton {

    String XPATH = "//div[contains(@class, 'modals__window--center')]";



    interface WithPopupWindow extends AbstractBlockElement {

        @FindBy(PopupWindow.XPATH)
        PopupWindow popup();

        @FindBy(".//text()[normalize-space(.) ='{{ name }}']/ancestor::*[self::button][1]|.//*[contains(@class,'button')and contains(text(),'{{ name }}')]")
        AtlasWebElement dynamicButton(@Param("buttonName") String buttonName);
    }

}
