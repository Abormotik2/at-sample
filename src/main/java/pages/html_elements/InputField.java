package pages.html_elements;


import io.qameta.atlas.core.api.Retry;
import io.qameta.atlas.webdriver.AtlasWebElement;
import io.qameta.atlas.webdriver.ElementsCollection;
import io.qameta.atlas.webdriver.extension.FindBy;
import io.qameta.atlas.webdriver.extension.Param;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.interactions.Actions;
import ru.vtb.at.Config;
import ru.vtb.at.context.Context;
import ru.vtb.at.driver.DriverManager;
import ru.vtb.at.pages.annotations.Title;
import ru.vtb.at.pages.block.AbstractBlockElement;
import ru.vtb.at.pages.element.UIElement;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;

@Title(value = "Поле ввода")
public interface InputField extends UIElement, AbstractBlockElement {


    default void sendKeys(LocalDate localDate) {
        sendKeys(localDate.format(DateTimeFormatter.ofPattern("dd.MM.yyyy")));
    }


    default void fillAndLoseFocus(String data) throws InterruptedException {
        fill(data);
        loseFocus();
    }

    default void fill(String data, String... args) {
        this.click();
        this.sendKeys(Keys.chord(Keys.HOME, Keys.LEFT_SHIFT, Keys.END, Keys.DELETE));
        this.sendKeys(Keys.HOME);
        if (Config.getStringSystemProperty("browser").equalsIgnoreCase("internet explorer")) {
            //todo обеспечить надежный ввод текста для IE
            for (char c : data.toCharArray()) {
                try {
                    Thread.sleep(250);
                    Actions action = new Actions(Context.getInstance().getBean(DriverManager.class).getDriver());
                    action.sendKeys(String.valueOf(c)).perform();
                    Thread.sleep(250);
                } catch (InterruptedException ignore) {
                }
            }
        } else {
            make().sendKeys(this, data);
        }
    }

    default void loseFocus() {
        make().loseFocus(this);
    }


    default boolean isReadOnly() {
        if (this.getAttribute("readonly") != null) {
            return this.getAttribute("readonly").equalsIgnoreCase("true");
        } else {
            return false;
        }
    }

    default String getValue() {
        return this.getAttribute("value");
    }

    @FindBy("./ancestor::*/span[starts-with(@class,'masked-input__button')]")
    UIElement catalogBtn();


    interface WithInput extends UIElement {
        @FindBy(".//textarea")
        ElementsCollection<InputField> inputField();

        @FindBy(".//input")
        ElementsCollection<InputField> inputs();

        @Retry(timeout = 21000, ignoring = NoSuchElementException.class)

        @FindBy(".//label[contains(text(),'{{ value }}')]/preceding-sibling::span/input|.//input[@placeholder='{{ value }}']")
        InputField input(@Param("value") String value);



        default InputField input() {
            InputField i = inputs().filter(AtlasWebElement::isDisplayed).waitUntil(hasSize(greaterThan(0))).get(0);
            return i;
        }
    }
}
