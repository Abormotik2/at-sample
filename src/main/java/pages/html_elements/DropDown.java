package pages.html_elements;

import assertion.AssertsManager;
import io.qameta.atlas.webdriver.AtlasWebElement;
import io.qameta.atlas.webdriver.ElementsCollection;
import io.qameta.atlas.webdriver.extension.FindBy;
import io.qameta.atlas.webdriver.extension.Name;
import io.qameta.atlas.webdriver.extension.Param;
import pages.vtb.pages.ModalWindowPage;
import ru.vtb.at.pages.annotations.Title;
import ru.vtb.at.pages.block.AbstractBlockElement;
import ru.vtb.at.pages.element.UIElement;

@Title(value = "ДропДаун")
public interface DropDown extends UIElement, AbstractBlockElement {

    @FindBy("//span[contains(@class, 'toggler__label')]")
    AtlasWebElement chosenInput();

    @FindBy("//ul[contains(@class, 'list-1m9')]")
    DropDown dropdownMenuField();

    @FindBy("//span[contains(@class, 'label__title')]")
    DropDown dropdownRow();

    @FindBy("//span[contains(@class, 'label__title')][text()='{{ name }}']")
    DropDown dropdownRow(@Param("name") String name);

    @FindBy("//div/li[@role='menuitem']//span[text()='{{ name }}']|//div/li[@role='menuitem'][text()='{{ name }}']")
    DropDown dropdownCreateFormRow(@Param("name") String name);

    @FindBy("//div/li[@role='menuitem']")
    DropDown dropMenuList();


    default void selectByValue(String value) {
        this.sendKeys(value);
    }

    default void selectByIndex(int index) {
        this.sendKeys();
    }

    default void selectMultipleItemsInDropdown(String... selectedLabels) {
        sendKeys(selectedLabels);
    }

    default String getSelectedInDropdownValue() {
        return getText();
    }

    default void selectByValues(String value) {
        this.click();
        this.select(value);
    }

    default void select(final String name) {
        if (dropdownMenuField().isDisplayed() || dropMenuList().isDisplayed()) {
            if (dropdownRow(name).isDisplayed()) {
                dropdownRow(name).click();
            } else if(dropdownCreateFormRow(name).isDisplayed()){
                System.out.println(dropdownCreateFormRow(name).isDisplayed());
                dropdownCreateFormRow(name).click();
            }
            else {
                AssertsManager.getAssertsManager().softAssert().fail("В списке выбора дропдауна нет искомого поля " + name);
            }
        }
        else{
            AssertsManager.getAssertsManager().softAssert().assertTrue(isDisplayed(), "Поле дропдауна не отображается");
        }
    }

    default String getValue() {
        return chosenInput().getText();
    }



    interface WithDropDown extends UIElement {
        @FindBy("//span[contains(@class, 'toggler__label')]")
        DropDown dropdown();
    }
}
