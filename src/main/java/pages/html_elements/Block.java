package pages.html_elements;

import ru.vtb.at.pages.block.AbstractBlockElement;
import ru.vtb.at.pages.element.UIElement;

public interface Block extends UIElement, AbstractBlockElement {

}