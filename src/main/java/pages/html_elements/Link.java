package pages.html_elements;

import io.qameta.atlas.webdriver.extension.FindBy;
import io.qameta.atlas.webdriver.extension.Param;
import ru.vtb.at.pages.annotations.Title;
import ru.vtb.at.pages.element.UIElement;

@Title(value = "Ссылка")
public interface Link extends UIElement {


    default String getLinkAddress() {
        return getAttribute("href");
    }


    public interface WithLink extends UIElement {
        @FindBy(".//a")
        Link link();

        @FindBy(".//a[.='{{ value }}']")
        Link linkWithText(@Param("value") String value);
    }
}
