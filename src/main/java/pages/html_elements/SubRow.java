package pages.html_elements;

import extensions.Is;
import io.qameta.atlas.webdriver.AtlasWebElement;
import io.qameta.atlas.webdriver.ElementsCollection;
import io.qameta.atlas.webdriver.extension.FindBy;
import ru.vtb.at.exceptions.FrameworkRuntimeException;
import ru.vtb.at.pages.block.AbstractBlockElement;

public interface SubRow extends AbstractBlockElement, InputField.WithInput, DropDown.WithDropDown, Button.WithButton {

    @FindBy(".//label")
    AtlasWebElement nameElement();

    @FindBy(".//label//span")
    AtlasWebElement textAreaNameElement();


    default InputField getInput(int inputNumberFrom_1) {
        if (inputNumberFrom_1 > inputs().size() || inputNumberFrom_1 < 1)
            throw new FrameworkRuntimeException("В строке " + getName() + " " + inputs().size() + " полей ввода. Поле № " + inputNumberFrom_1 + " в строке отсутствует");
        return inputs().get(inputNumberFrom_1 - 1);
    }

    default InputField getInputField(int inputNumberFrom_1) {
        if (inputNumberFrom_1 > inputField().size() || inputNumberFrom_1 < 1)
            throw new FrameworkRuntimeException("В строке " + getName() + " " + inputField().size() + " полей ввода. Поле № " + inputNumberFrom_1 + " в строке отсутствует");
        return inputField().get(inputNumberFrom_1 - 1);
    }

    default InputField getInput() {
        return getInput(1);

    }

    default InputField getInputField() {
        return getInputField(1);

    }

    default String getName() {
        if (nameElement().isDisplayed()) {
            if (Is.visible(nameElement()))
                return nameElement().getText().replace("*","").replace("\n", "").trim();

            else
                return this.getText().replace("*","").replace("\n", "").trim();
        } else {
            return "";
        }
    }

    default String getTextAreaName() {
        if (textAreaNameElement().isDisplayed()) {
            if (Is.visible(textAreaNameElement()))
                return textAreaNameElement().getText().replace("*","").replace("\n", "").trim();
            else
                return this.getText().replace("*","").replace("\n", "").trim();
        } else {
            return "";
        }
    }


    interface WithSubRows extends AbstractBlockElement {
        @FindBy(".//div[contains(@class, 'grid__col')]")
        ElementsCollection<SubRow> subrows();
    }
}
