import com.epam.reportportal.testng.ReportPortalTestNGListener;
import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;
import cucumber.api.testng.CucumberFeatureWrapper;
import cucumber.api.testng.PickleEventWrapper;
import cucumber.api.testng.TestNGCucumberRunner;
import org.testng.ITest;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import utils.allure.AllureEnvironmentSaver;

import java.lang.reflect.Method;

@CucumberOptions(
        plugin = {"pretty",
                "io.qameta.allure.cucumber4jvm.AllureCucumber4Jvm",
                "json:target/cucumber.json",
        },
        glue = {"steps", "hooks"},
        features = "classpath:features"
)
@Listeners({ReportPortalTestNGListener.class})
public class TestsRunner extends AbstractTestNGCucumberTests implements ITest {
    private TestNGCucumberRunner testNGCucumberRunner;
    protected static ThreadLocal<String> mTestCaseName = new ThreadLocal<>();

    @Override
    public void runScenario(PickleEventWrapper pickleWrapper, CucumberFeatureWrapper featureWrapper) throws Throwable {
        super.runScenario(pickleWrapper, featureWrapper);
    }

    @Override
    @DataProvider(parallel = true)
    public Object[][] scenarios() {
        System.out.println("<<<<<Scenarios count:" + super.scenarios().length);
        return super.scenarios();
    }

    @Override
    @AfterClass(alwaysRun = true)
    public void tearDownClass() throws Exception {
        AllureEnvironmentSaver.save();
        super.tearDownClass();
    }

    @BeforeMethod
    public void beforeMethod(Method name, Object[] testData) {
        mTestCaseName.set(testData[0].toString());
    }

    @Override
    public String getTestName() {
        return mTestCaseName.get();
    }
}
