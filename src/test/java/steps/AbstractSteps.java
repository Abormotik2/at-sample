package steps;


import pages.vtb.pages.ModalWindowPage;
import pages.vtb.pages.blocks.FormBlockElement;
import ru.vtb.at.exceptions.FrameworkRuntimeException;

public class AbstractSteps extends BaseSteps {

    static final String CURRENT_BLOCK = "current block";
    static final String CURRENT_BLOCK_NAME = "current.block.name";


    public ModalWindowPage getModalWindowPage() {
        return getPage(ModalWindowPage.class);
    }

    protected FormBlockElement getCurrentBlock() {
        FormBlockElement currentBlockName = getTestData(CURRENT_BLOCK);
        if (currentBlockName != null)
            return currentBlockName;
        else
            throw new FrameworkRuntimeException("Не выбран текущий блок");
    }

    protected void setCurrentBlock (FormBlockElement formBlockElement) {
        if (formBlockElement != null) {
            saveTestData(CURRENT_BLOCK, formBlockElement);
            saveTestData(CURRENT_BLOCK_NAME, formBlockElement.getName());
        } else {
            throw new FrameworkRuntimeException("Невозожно установить текущий блок, новая ссылка на блок пуста");
        }
    }
}