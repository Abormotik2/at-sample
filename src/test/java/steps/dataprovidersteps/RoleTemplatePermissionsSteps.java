package steps.dataprovidersteps;

import cucumber.api.java.ru.И;
import cucumber.api.java.ru.Пусть;
import io.cucumber.datatable.DataTable;
import ru.vtb.at.DataValidatorClient;
import ru.vtb.at.businessmodel.client.Client;
import ru.vtb.at.businessmodel.user.Permissions;
import ru.vtb.at.businessmodel.user.Role;
import ru.vtb.at.businessmodel.user.RoleTemplate;
import steps.BaseSteps;

import java.util.HashMap;

public class RoleTemplatePermissionsSteps extends BaseSteps {

    @Пусть("я создал Шаблон Роли {string} со следующими правами:")
    public void shouldCreateTemplateWithPermissions(String templateName, DataTable desiredPermissions) {
        HashMap<String, String> prm =  new HashMap<>();
        desiredPermissions.subTable(1,0).asLists().forEach(strings -> {
                    String businessMethod = strings.get(2);
                    String businessObject = strings.get(1);
                    prm.put(businessMethod, businessObject);
                });
        Permissions permissions = new Permissions(prm);
        RoleTemplate roleTemplate = new RoleTemplate(templateName, permissions);
        setCitrusVariable(templateName, roleTemplate);
        setCitrusVariable("roleTemplate", roleTemplate);
    }

    @Пусть("обновил Роль {string} следующими правами:")
    public void shouldUpdateRolePermissions(String roleName, DataTable desiredPermissions) {
        Client client = getCitrusVariable("CLIENT", Client.class);
        RoleTemplate roleTemplate = getCitrusVariable("roleTemplate", RoleTemplate.class);
        HashMap<String, String> prm =  new HashMap<>();
        desiredPermissions.subTable(1,0).asLists().forEach(strings -> {
            String businessMethod = strings.get(2);
            String businessObject = strings.get(1);
            prm.put(businessMethod, businessObject);
        });
        Permissions permissions = new Permissions(prm);

        Role role = new Role(client.getId(), roleName, roleTemplate, permissions, false);

        DataValidatorClient.validateRole(role);
        setCitrusVariable(roleName, role);
    }

    @Пусть("я создал в ДБО Шаблон Роли {string} со следующими правами:")
    public void shouldCreateInDBOTemplateWithPermissions(String templateName, DataTable desiredPermissions) {
        HashMap<String, String> prm =  new HashMap<>();
        desiredPermissions.subTable(1,0).asLists().forEach(strings -> {
            String businessMethod = strings.get(2);
            String businessObject = strings.get(1);
            prm.put(businessMethod, businessObject);
        });
        Permissions permissions = new Permissions(prm);
        RoleTemplate roleTemplate = new RoleTemplate(templateName, permissions);

        DataValidatorClient.validateRoleTemplate(roleTemplate);

        setCitrusVariable(templateName, roleTemplate);
        setCitrusVariable("roleTemplate", roleTemplate);
    }

    @И("создал в ДБО Роль {string} на основе Шаблона {string} для полученной организации")
    public void shouldCreateRoleByTemplate(String roleName, String templateName) {
        RoleTemplate roleTemplate = getCitrusVariable(templateName, RoleTemplate.class);
        Client client = getCitrusVariable("CLIENT", Client.class);
        Role role = new Role(client.getId(), roleName, roleTemplate);
        DataValidatorClient.validateRole(role);
        roleTemplate.setSignature(role.getSignature());
        setCitrusVariable(roleName, role);
    }

    @И("создал в ДБО Роль {string} на основе нового Шаблона {string} для полученной организации")
    public void shouldCreateRoleByNewTemplate(String roleName, String templateName) {
        RoleTemplate roleTemplate = new RoleTemplate(templateName);
        Client client = getCitrusVariable("CLIENT", Client.class);
        Role role = new Role(client.getId(), roleName, roleTemplate);
        DataValidatorClient.validateRole(role);
        roleTemplate.setSignature(role.getSignature());
        setCitrusVariable(roleName, role);
        setCitrusVariable("roleTemplate", roleTemplate);
    }
}
