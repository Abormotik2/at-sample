package steps.dataprovidersteps;

import assertion.AssertsManager;
import cucumber.api.java.ru.И;
import cucumber.api.java.ru.Пусть;
import cucumber.api.java.ru.Также;
import io.cucumber.datatable.DataTable;
import ru.vtb.at.DataProviderClient;
import ru.vtb.at.DataValidatorClient;
import ru.vtb.at.businessmodel.client.Client;
import ru.vtb.at.businessmodel.signature.Employee;
import ru.vtb.at.businessmodel.signature.Sign;
import ru.vtb.at.businessmodel.signature.SignModel;
import ru.vtb.at.businessmodel.signature.Signatures;
import ru.vtb.at.businessmodel.user.Role;
import ru.vtb.at.businessmodel.user.User;
import steps.BaseSteps;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class DataProviderClientSteps extends BaseSteps {

    @Пусть("я получил случайную организацию")
    public void shouldGetRandomClient() {
        Client client = DataProviderClient.getRandomClient();
        DataValidatorClient.validateClient(client);
        setCitrusVariable("CLIENT", client);
    }

    @Пусть("я получил случайную организацию и добавил к ней роли из списка")
    public void shouldGetRandomClientSetRoles(List<String> roleNames) {
        Client client = DataProviderClient.getRandomClient();
        String id = DataValidatorClient.validateClient(client);

        List<Role> rolesFromDP = DataProviderClient.getRolesByName(roleNames);

        for (Role role : rolesFromDP) {
            role.setClientId(id);
            role.setId(DataValidatorClient.validateRole(role));
        }

        client.setRoles(rolesFromDP);
        setCitrusVariable("CLIENT", client);
    }

    @И("привязал пользователя к полученной организации с ролью {string}")
    public void shouldSetClientNotDefaultRolesForBinding(String roleName) {
        Client client = getCitrusVariable("CLIENT", Client.class);
        ArrayList<Role> expRoles = new ArrayList<>(); //список ожидаемых ролей клиента

        Role role = getCitrusVariable(roleName, Role.class);
        expRoles.add(role);

        HashMap<Client, List<Role>> clientListHashMap = new HashMap<>();
        clientListHashMap.put(client, expRoles);

        setCitrusVariable("ClientRoleMap", clientListHashMap);
    }


     @И("выбрал среди доступных ролей клиента те, что необходимы для привязки пользователя")
        public void shouldSetClientRolesForBinding(List<String> desiredRoleNames) {
         Client client = getCitrusVariable("CLIENT", Client.class);

         ArrayList<Role> expRoles = new ArrayList<>(); //список ожидаемых ролей клиента

         for (Role role : client.getRoles()) {
             if (desiredRoleNames.contains(role.getDbo_name())) {
                 expRoles.add(role);
             }
         }

         HashMap<Client, List<Role>> clientListHashMap = new HashMap<>();
         clientListHashMap.put(client, expRoles);

         setCitrusVariable("ClientRoleMap", clientListHashMap);
        }

    @Также("я получил случайного пользователя")
    public void shouldGetRandomUser() {
        User user = DataProviderClient.getRandomUser();
        String uid = DataValidatorClient.validateUser(user);
        user.setId(uid);
        setCitrusVariable("USER", user);
    }

    @И("создал связь между организацией и пользователем")
    public void shouldLinkClientToUser() {
        DataValidatorClient.linkClientsAndUserFromDp(
                getCitrusVariable("USER", User.class),
                getCitrusVariable("ClientRoleMap", HashMap.class)
        );
    }

    @И("установил пользователя в качестве работника организации и внес его в список {string}")
    public void shouldSetEmployee(String employeeListName) {
        User user = getCitrusVariable("USER", User.class);
        Employee employee = new Employee(user.getId(), user.getFio());
        List<Employee> employeeList = new ArrayList<>();
        employeeList.add(employee);
        setCitrusVariable(employeeListName, employeeList);
    }

    @И("установил любого пользователя в качестве работника организации и внес его в список {string}")
    public void shouldSetNonSpecificEmployee(String employeeListName) {
        Employee nonSpecificEmployee = new Employee(null, null);
        List<Employee> employeeList = new ArrayList<>();
        employeeList.add(nonSpecificEmployee);
        setCitrusVariable(employeeListName, employeeList);
    }

    @И("указал необходимое количество подписей {int} и параметры к ним:")
    public void shouldSetParametersToSign(int numberOfSigns, DataTable dataTable) {
        if (numberOfSigns == dataTable.height()) {
            List<Sign> signs = new ArrayList<>();
            dataTable.asLists().forEach(strings -> {
                String employeeListName = strings.get(0);
                List<Employee> employeeList = getCitrusVariable(employeeListName, List.class);

                Boolean waitAll = Boolean.valueOf(strings.get(1));

                Sign.SignType signType = Sign.SignType.valueOf(strings.get(2));

                Sign sign = new Sign(employeeList, waitAll, signType);

                signs.add(sign);
            });

            setCitrusVariable("signs", signs);
        }
        else {
            AssertsManager.getAssertsManager().criticalAssert().assertEquals(numberOfSigns, dataTable.height(),
                    "Установлены неверные входные данные: количество задаваемых подписей должно быть равно количеству строк задаваемых в таблице!");
        }
    }

    @И("добавил сигнатуры и установил нужную модель подписания")
    public void shouldSetSignModel() {
        Signatures signatures = new Signatures(getCitrusVariable("signs", List.class));

        SignModel signModel = new SignModel(SignModel.ModelType.CLIENT,
                            getCitrusVariable("signs", List.class).size(), signatures);

        DataValidatorClient.normalizeBindingSignModels(getCitrusVariable("CLIENT", Client.class), signModel);
    }
}
