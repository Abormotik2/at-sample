package steps.dataprovidersteps;

import cucumber.api.java.ru.И;
import cucumber.api.java.ru.Пусть;
import org.openqa.selenium.MutableCapabilities;
import pages.vtb.pages.VtbAuthorizationPage;
import ru.vtb.at.DataProviderClient;
import ru.vtb.at.DataValidatorClient;
import ru.vtb.at.businessmodel.certificate.BrowserCertificate;
import ru.vtb.at.businessmodel.client.Client;
import ru.vtb.at.businessmodel.user.User;
import steps.BaseSteps;
import utils.CSPUtils;
import utils.kubernetes.Kubernetes;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class CertSampleSteps extends BaseSteps {

    @Пусть("я получил случайного пользователя и установил его сертификат в систему")
    public void shouldGetRandomUserInstallCert() throws IOException {
        User user = DataProviderClient.getRandomUser();
        String uid = DataValidatorClient.validateUser(user);
        user.setId(uid);
        setCitrusVariable("USER", user);

        BrowserCertificate certificate =
                new BrowserCertificate(user.getCert_name(), user.getCert_common_name(),
                        user.getCertificate_number(), user.getUser_name(), user.getLocked(),
                        user.getUid(), user.getPublic_key(), user.getPrivate_key(), user.getPassword());

        CSPUtils.installCertificateFromDPIfNotExist(certificate);
        setCitrusVariable("CERTIFICATE", certificate);
    }

    @И("авторизовался с помощью сертификата пользователя в ДБО")
    public void shouldAuthDBOByUserCert() {
        BrowserCertificate certificate = getCitrusVariable("CERTIFICATE", BrowserCertificate.class);
        setCapabilities(certificate);
        openMainPage();
        getPageByTitle("Страница авторизации");
        resetCurrentBlock();
        VtbAuthorizationPage page = getPage(VtbAuthorizationPage.class);
        page.authByCertificate(certificate.getCert_common_name(), certificate.getPassword());
    }


    private void openMainPage() {
        getDriver().navigate().to(Kubernetes.ingresses.getUrl("fe-root-client-iteration"));
    }

    private void setCapabilities(BrowserCertificate certificate) {
        Map<String, Object> caps = new HashMap<>();
        List<String> certs = new ArrayList<>();
        certs.add("CERTS_MASK=" + certificate.getCert_name());
        if (Kubernetes.ingresses.getUrl("fe-root-client-iteration") != null) certs.add("TRUSTED_SITES=" + Kubernetes.ingresses.getUrl("fe-root-client-iteration"));
        caps.put("env", certs);
        caps.put("timeZone", "Europe/Moscow");
        getDriverManager().addCapabilities(new MutableCapabilities(caps));
    }


    @И("привязал сертификат пользователя к выбранной организации")
    public void shouldBindCertToOrg() {
        User user = getCitrusVariable("USER",User.class);
        BrowserCertificate browserCertificate =
                new BrowserCertificate(user.getCert_name(), user.getCert_common_name(),
                        user.getCertificate_number(), user.getUser_name(), user.getLocked(),
                        user.getUid(), user.getPublic_key(), user.getPrivate_key(), user.getPassword());

        DataValidatorClient.normalizeBindingCertToOrg(browserCertificate, getCitrusVariable("CLIENT", Client.class));
    }
}
