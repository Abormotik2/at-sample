package steps.api;


import cucumber.api.java.ru.И;
import steps.BaseSteps;
import utils.ApiClient;

import java.util.HashMap;


public class ApiAuthSteps extends BaseSteps {

    @И("я отправляю запрос {string} и сохраняю ответ в {string}")
    public void adminAuth(String serviceName, String variable) {
        setCitrusVariable(variable, getApiClient().getDataStringFromService(serviceName, new HashMap<>(), null));
    }

//    @И("получил токен активной сессии по логину {string} и паролю {string}")
//    public String getUserAuthTokenByLoginPswrd(String login, String password) {
//        return new ApiClient().getUserAuthTokenByLoginPassword(login, password);
//    }
}