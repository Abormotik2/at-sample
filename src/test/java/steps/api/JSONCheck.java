package steps.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import cucumber.api.java.ru.И;
import io.qameta.allure.Allure;
import steps.BaseSteps;

import java.io.IOException;
import java.util.*;

public class JSONCheck extends BaseSteps {
    @И("для каждого элемента в {string} присутствуют не пустые поля:")
    public void inJSONFieldsExisted(String var, List<String> fields) throws IOException {
        //TODO написать номральную проверку со структурами!
        String response = getCitrusVariable(var);
        Map data = new ObjectMapper().readValue(getCitrusVariable("doc-types"), HashMap.class);
        for (LinkedHashMap elem : (ArrayList<LinkedHashMap>) data.get("data")) {
            Allure.addAttachment("Проверка " + elem.get("name").toString(), elem.toString());
            fields.forEach(category -> {
                assertsManager.softAssert().assertTrue(elem.get(category) != null);
                if (elem.get(category) != null) {
                    assertsManager.softAssert().assertTrue(elem.get(category) != "");
                }
            });
        }
    }
}

