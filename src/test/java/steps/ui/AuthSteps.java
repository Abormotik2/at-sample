package steps.ui;

import cucumber.api.java.ru.Дано;
import cucumber.api.java.ru.И;
import org.json.JSONObject;
import org.openqa.selenium.MutableCapabilities;
import pages.vtb.pages.VtbAuthorizationPage;
import pages.vtb.pages.ru.Страница_авторизации;
import ru.vtb.at.DataProviderClient;
import ru.vtb.at.businessmodel.certificate.BrowserCertificate;
import steps.AbstractSteps;
import utils.CSPUtils;
import utils.Stash;
import utils.kubernetes.Kubernetes;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AuthSteps extends AbstractSteps {

    public static final String FREE_CERT = "свободный сертификат";
    public static final String DEFAULT_PASSWORD = "!QAZ2wsx";

    @Дано("UI авторизуемся пользователем по сертификату")
    public void authByCertificate() throws IOException {
        BrowserCertificate certificate = DataProviderClient.getRandomCertificate(); //Получаем сертификат
        CSPUtils.installCertificateFromDPIfNotExist(certificate); //устанавливаем сертификат
        setCapabilities(certificate); //устанавливаем capabilities для запуска
        openMainPage();
        getPageByTitle("Страница авторизации");
        resetCurrentBlock();
        VtbAuthorizationPage page = getPage(VtbAuthorizationPage.class);
        page.authByCertificate(certificate.getCert_common_name(), certificate.getPassword());
    }

    /**
     *
     * @param stashUser сейчас передаётся логин
     * @throws IOException
     */
    @И("\"(.+)\" авторизуется по логину")
    public void authByLogin(String stashUser) throws IOException {
//        TODO: реализовать поиск логина в юзере
        JSONObject stashedUserData = (JSONObject) Stash.getStashInstance().getStashIfExist(stashUser);
        openMainPage();
        this.<Страница_авторизации>getPageByTitle("ВТБ Бизнес-онлайн").блок_авторизации().authByLogin(stashedUserData.getString("login"), DEFAULT_PASSWORD);
    }

    private void openMainPage() {
        getDriver().navigate().to(Kubernetes.ingresses.getUrl("fe-root-client-iteration"));
    }

    private void setCapabilities(BrowserCertificate certificate) {
        Map<String, Object> caps = new HashMap<>();
        List<String> certs = new ArrayList<>();
        certs.add("CERTS_MASK=" + certificate.getCert_name());
        if (Kubernetes.ingresses.getUrl("fe-root-client-iteration") != null) certs.add("TRUSTED_SITES=" + Kubernetes.ingresses.getUrl("fe-root-client-iteration"));
        caps.put("env", certs);
        caps.put("timeZone", "Europe/Moscow");
        getDriverManager().addCapabilities(new MutableCapabilities(caps));
    }
}
