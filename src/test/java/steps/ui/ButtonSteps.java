package steps.ui;

import cucumber.api.java.ru.И;
import pages.vtb.pages.CreateMenuPage;
import pages.vtb.pages.VtbMainPage;
import steps.AbstractSteps;


public class ButtonSteps extends AbstractSteps {

    @И(("нажимает на кнопку {string}"))
    public void clickButton(String buttonName) {
        VtbMainPage vtbMainPage = getPage(VtbMainPage.class);
        vtbMainPage.button(buttonName).click();
    }

    @И(("в поле {string} нажать на кнопку"))
    public void sendKeysToTextAreaByName(String fieldName) {
        getModalWindowPage().modalFormContent().getSubRows(fieldName).maskedButton().click();
    }

    @И(("нажимает в меню 'Создать' на {string}"))
    public void clickButtonMenu(String buttonName) {
        CreateMenuPage createMenuPage = getPage(CreateMenuPage.class);
        createMenuPage.createMenuBlock().button(buttonName).click();
    }

    @И(("в правом меню нажимает на кнопку {string}"))
    public void clickSideBarButton(String buttonName) {
        getModalWindowPage().modalFormSidebar().button(buttonName).click();
    }

    @И("кнопка {string} активна")
    public void buttonIsActive(String buttonName) {
        VtbMainPage vtbMainPage = getPage(VtbMainPage.class);
        assertsManager.criticalAssert().assertTrue(vtbMainPage.button(buttonName).isDisplayed());
        assertsManager.criticalAssert().assertTrue(vtbMainPage.button(buttonName).isEnabled());
    }

    //перенести в общие шаги
    @И(("подождать {int} секунд"))
    public void waitSeconds(int delay) throws InterruptedException {
        log.trace("Ждём {} секунд", delay);
        Thread.sleep(delay * 1000);
    }
}


