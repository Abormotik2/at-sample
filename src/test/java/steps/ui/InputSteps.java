package steps.ui;

import cucumber.api.java.ru.И;
import pages.html_elements.InputField;
import pages.html_elements.SubRow;
import pages.vtb.pages.ModalWindowPage;
import ru.vtb.at.make.Make;
import steps.AbstractSteps;

public class InputSteps extends AbstractSteps {

    @И(("в поле {string} ввести {string}"))
    public void sendKeysToFieldByName(String fieldName, String value) throws InterruptedException {
        getModalWindowPage().modalFormContent().getSubRows(fieldName).getInput().fill(value, Make.SLOW_INPUT);
    }

    @И(("в поле {string} введено {string}"))
    public void fieldByNameIsFilled(String fieldName, String expValue) {
        String actualValue = getModalWindowPage().modalFormContent().getSubRows(fieldName).getInput().getValue();
        assertsManager.softAssert().assertEquals(actualValue, expValue, "В поле " + fieldName + " введено неверное значение: " + actualValue);
    }

    @И(("в текстовом поле {string} ввести {string}"))
    public void sendKeysToTextAreaByName(String fieldName, String value) throws InterruptedException {
        getModalWindowPage().modalFormContent().getSubRowsTextArea(fieldName).getInputField().fill(value);
    }

    @И(("в текстовом поле {string} введено {string}"))
    public void TextAreaByNameIsFilled(String fieldName, String expValue) {
        String actualValue = getModalWindowPage().modalFormContent().getSubRowsTextArea(fieldName).getInputField().getValue();
        assertsManager.softAssert().assertEquals(actualValue, expValue, "В текстовом поле " + fieldName + " введено неверное значение: " + actualValue);
    }

    @И(("в текущем блоке в поле {string} ввести {string}"))
    public void sendKeysToCurrentBlockFieldByName(String fieldName, String value) throws InterruptedException {
        getCurrentBlock().getSubRows(fieldName).getInput().fill(value);
    }

    @И(("в текущем блоке в поле {string} введено {string}"))
    public void fieldByNameInCurrentBlockIsFilled(String fieldName, String expValue) {
        String actualValue = getCurrentBlock().getSubRows(fieldName).getInput().getValue();
        assertsManager.softAssert().assertEquals(actualValue, expValue, "В теущем блоке в поле " + fieldName + " введено неверное значение: " + actualValue);
    }


    @И(("в текущем блоке в текстовом поле {string} ввести {string}"))
    public void sendKeysInBlockToTextAreaByName(String fieldName, String value) throws InterruptedException {
        getCurrentBlock().getSubRowsTextArea(fieldName).getInputField().fill(value);
    }


    @И(("в текущем блоке в текстовом поле {string} введено {string}"))
    public void textAreaInBlockByNameIsFilled(String fieldName, String expValue) {
        String actualValue = getCurrentBlock().getSubRowsTextArea(fieldName).getInputField().getValue();
        assertsManager.softAssert().assertEquals(actualValue, expValue, "В текстовом поле " + fieldName + " введено неверное значение: " + actualValue);
    }


    @И(("в {int} поле {string} ввести {string}"))
    public void fieldByNameIsFilled(int subFieldNumber, String fieldName, String value) throws InterruptedException {
        getModalWindowPage().modalFormContent().getSubRows(fieldName).getInput(subFieldNumber).fill(value);
    }


    @И(("в {int} похожее поле {string} ввести {string}"))
    public void similarFieldByNameSendKeys(int fieldPos, String fieldName, String value) throws InterruptedException {
        getModalWindowPage().modalFormContent().getSubRowsByName(fieldName).get(fieldPos - 1).getInput().fill(value);
    }


    @И(("в {int} похожее поле {string} введено {string}"))
    public void similarFieldByNameIsFilled(int fieldPos, String fieldName, String expValue) {
        String actualValue = getModalWindowPage().modalFormContent().getSubRowsByName(fieldName).get(fieldPos - 1).getInput().getValue();
        assertsManager.softAssert().assertEquals(actualValue, expValue, "В " + fieldPos + " похожем поле " + fieldName + " введено неверное значение: " + actualValue);
    }

}

