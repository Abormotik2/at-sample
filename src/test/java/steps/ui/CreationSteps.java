package steps.ui;

import cucumber.api.java.ru.Если;
import cucumber.api.java.ru.И;
import io.cucumber.datatable.DataTable;
import pages.vtb.pages.CreateMenuPage;
import pages.vtb.pages.ModalWindowPage;
import pages.vtb.pages.VtbMainPage;
import steps.AbstractSteps;

import java.util.List;

public class CreationSteps extends AbstractSteps {

    @Если("создать {string} c параметрами:")
    public void создать(String menuItem, DataTable dataTable) {
        getPage(VtbMainPage.class).headerBlock().button("Создать").click(); //Открываем меню создать
        getPage(CreateMenuPage.class).createMenuBlock().button(menuItem).click(); //Выбираем из меню нужный элемент
        dataTable.asLists().forEach(strings -> {
            String block = strings.get(0);
            String field = strings.get(1);
            String value = strings.get(2);
            if (block.isEmpty()) {
                getModalWindowPage().modalFormContent().getSubRows(field).getInput().fill(value);
                ;
            } else {
                getModalWindowPage().modalFormContent().getChildBlock(block).getSubRows(field).getInput().fill(value);
                ;

            }
        });
    }

    @И("на скроллере присутствуют вкладки")
    public void tabs(List<String> tabList) {
        ModalWindowPage page = getPage(ModalWindowPage.class);
        tabList.forEach(x -> page.tabsList().filter(z -> z.getText().equals(x)).get(0).isDisplayed());
    }

    @И("сохраняет документ")
    public void saveDoc(){
        getModalWindowPage().modalFormSidebar().button("Сохранить").click();
        if (getModalWindowPage().popUpTitle().isDisplayed()) {
            getModalWindowPage().closePopUpTitle().click();
        }
    }
}
