package steps.ui;

import assertion.AssertsManager;
import cucumber.api.java.ru.И;
import pages.html_elements.DropDown;
import pages.vtb.pages.ModalWindowPage;
import steps.AbstractSteps;

public class DropdownSteps extends AbstractSteps {

    @И(("в поле {string} выбрать {string}"))
    public void sendKeysToDropdownByName(String fieldName, String value) {
        getModalWindowPage().modalFormContent().getSubRowsTextArea(fieldName).dropdown().selectByValues(value);
    }

    @И(("в поле {string} выбрано {string}"))
    public void dropdownByNameIsFilledByValue(String fieldName, String expValue) {
        String actualValue = getModalWindowPage().modalFormContent().getSubRowsTextArea(fieldName).dropdown().getValue();
        AssertsManager.getAssertsManager().softAssert().assertEquals(actualValue, expValue, "В поле " + fieldName + " выбрано другое значение");
    }

    @И(("в текущем блоке в поле {string} выбрать {string}"))
    public void sendKeysInBlockToDropdownByName(String fieldName, String value) {
        getCurrentBlock().getSubRowsTextArea(fieldName).dropdown().selectByValues(value);
    }

    @И(("в текущем блоке в поле {string} выбрано {string}"))
    public void dropdownByNameInBlockIsFilledByValue(String fieldName, String exvalue) {
        String actualValue = getCurrentBlock().getSubRowsTextArea(fieldName).dropdown().getValue();
        AssertsManager.getAssertsManager().softAssert().assertEquals(actualValue, exvalue, "В текущем блоке в поле " + fieldName + " выбрано другое значение");
    }
}