package steps.ui;

import cucumber.api.java.ru.И;
import pages.html_elements.Row;
import pages.vtb.pages.RrkoPage;
import pages.vtb.pages.VtbMainPage;
import pages.vtb.pages.blocks.ActionElement;
import steps.AbstractSteps;


public class TableSteps extends AbstractSteps {

    @И("выделить документ {string}")
    public void selectDocument(String docNumber){
        RrkoPage page = getPageByTitle("Платёжные поручения");
        Row foundRow = findRow(page,docNumber);
        page.checkRow(foundRow);
        assertsManager.criticalAssert().assertTrue(foundRow.checkBox().isChecked());
    }

    @И("выделить документ в статусе {string}")
    public void selectDocumentStatus(String status){
        RrkoPage page = getPageByTitle("Платёжные поручения");
        Row foundRow = findRow(page,status);
        page.checkRow(foundRow);
        assertsManager.criticalAssert().assertTrue(foundRow.checkBox().isChecked());
    }

    @И("снять выделение с документа {string}")
    public void unSelectDocument(String docNumber){
        RrkoPage page = getPageByTitle("Платёжные поручения");
        Row foundRow = findRow(page,docNumber);
        page.unCheckRow(foundRow);
        assertsManager.criticalAssert().assertFalse(foundRow.checkBox().isChecked());
    }

    @И("снять все выделения")
    public void clearSelect(){
        VtbMainPage page = getPageByTitle("Главная страница");
        page.actionLine().customCheckBox().setChecked(false);
        assertsManager.criticalAssert().assertFalse(page.actionLine().customCheckBox().isChecked());
    }

    @И("перейти на скроллере на вкладку {string}")
    public void moveToScrollTab(String tabName){
        RrkoPage page = getPageByTitle("Платёжные поручения");
        page.getTab(tabName).click();
    }

    @И("документ {string} присутствует в таблице")
    public void isDocExist(String docNumber){
        RrkoPage page = getPageByTitle("Платёжные поручения");
        assertsManager.criticalAssert().assertTrue(findRow(page,docNumber).isDisplayed());
    }

    @И("документ {string} не присутствует в таблице")
    public void isDocNotExist(String docNumber){
        RrkoPage page = getPageByTitle("Платёжные поручения");
        assertsManager.criticalAssert().assertFalse(page.hasRow(docNumber));
    }

    private Row findRow(RrkoPage page, String docNumber) {
        if ("случайный документ".equals(docNumber))
            return page.getRandomRow();
        else
            return page.getRow(docNumber);
    }
}
