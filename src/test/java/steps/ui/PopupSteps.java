package steps.ui;

import assertion.AssertsManager;
import cucumber.api.java.bg.И;
import pages.html_elements.PopupWindow;
import pages.vtb.pages.ModalWindowPage;
import steps.AbstractSteps;
import utils.Wait;

public class PopupSteps extends AbstractSteps {

    @И(("во всплывающем окне нажать {string}"))
    public void clickButtonInPopupWindow(String button) {
        Wait.until(() -> getPopup().isDisplayed(),10, 1);
        if (getPopup().isDisplayed()) {
            getPopup().button(button).click();
        } else {
            AssertsManager.getAssertsManager().softAssert().fail("Попап сообщение отсутствует");
        }
    }

    public PopupWindow getPopup() {
        return getModalWindowPage().popup();
    }

}

