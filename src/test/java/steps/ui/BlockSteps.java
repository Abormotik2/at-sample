package steps.ui;

import cucumber.api.java.bg.И;
import pages.vtb.pages.CreateMenuPage;
import pages.vtb.pages.VtbMainPage;
import pages.vtb.pages.blocks.CreateMenuBlock;
import pages.vtb.pages.blocks.FormBlockElement;
import pages.vtb.pages.blocks.SidebarBlock;
import ru.vtb.at.pages.AbstractPage;
import steps.AbstractSteps;

public class BlockSteps extends AbstractSteps {

    @И(("перейти к блоку {string}"))
    public void moveToBlock(String blockName) {
        FormBlockElement block = getModalWindowPage().getChildBlock(blockName);
        assertsManager.softAssert().assertTrue(block.isDisplayed(), "Блок " + blockName + " не отображается");
        setCurrentBlock(block);
    }

    @И("перейти в раздел {string} на вкладку {string}")
    public void moveToSection(String section,String tab){
        SidebarBlock page = getPage(SidebarBlock.class);
        page.text(section).click();
        page.text(tab).click();
    }

}
