package steps;


import cucumber.api.java.ru.И;
import io.cucumber.datatable.DataTable;
import org.json.JSONObject;
import pages.vtb.pages.CommonPage;
import pages.vtb.pages.ImportPage;
import pages.vtb.pages.ModalWindowPage;
import pages.vtb.pages.blocks.HeaderBlock;
import ru.vtb.at.DataValidatorClient;
import ru.vtb.at.businessmodel.client.Client;
import ru.vtb.at.businessmodel.user.User;
import utils.ApiClient;
import utils.Stash;
import utils.Wait;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

import static ru.vtb.at.DataProviderClient.getRandomClient;
import static ru.vtb.at.DataProviderClient.getRandomUser;
import static ru.vtb.at.DataValidatorClient.validateUser;

public class StepDefs extends AbstractSteps {

    @И("организация {string} с параметрами")
    public void getOrganization(String organization, DataTable params) {

    }

    @И("организация {string}")
    public void getOrganization(String organization) {
        Client foundClient = getRandomClient();
        if (foundClient != null) {
            DataValidatorClient.validateClient(foundClient);
            Stash.getStashInstance().addStashObj(organization, foundClient);
        } else {
            throw new NullPointerException();
        }
    }

    @И("заполняет поля получателя при создании документа")
    public void fillDocumentReceiver() {
        Client receiver = (Client) Stash.getStashInstance().getStashIfExist("получатель");
        ModalWindowPage page = getPage(ModalWindowPage.class);
        page.modalFormContent().inputs().filter(x -> x.getAttribute("name").equals("receiver.innKio")).get(0).fill(receiver.getInn());
        page.modalFormContent().inputs().filter(x -> x.getAttribute("name").equals("receiver.account")).get(0).fill(receiver.getAccounts().get(0).getAccount_number());
        page.modalFormContent().inputs().filter(x -> x.getAttribute("name").equals("receiver.bank.bic")).get(0).fill(receiver.getBranches().get(0).getHeadBranch().getBic());
        if (page.modalFormContent().dropdown().dropMenuList().isDisplayed()) {
            page.dropdown().select(page.modalFormContent().inputs().filter(x -> x.getAttribute("name").equals("receiver.bank.bic")).get(0).getValue());
        }
        page.modalFormContent().inputField().filter(x -> x.getAttribute("name").equals("receiver.name")).get(0).fill(receiver.getShort_name());
        page.modalFormContent().inputField().filter(x -> x.getAttribute("name").equals("paymentGround.description")).get(0).fill("НДС 20%");
    }

    @И("заполняет поля плательщика при создании документа")
    public void fillDocument() {
        Client client = (Client) Stash.getStashInstance().getStashIfExist("плательщик");
        ModalWindowPage page = getPage(ModalWindowPage.class);
        Wait.waitToBeClicable(page.modalFormContent().inputs().get(0));
        page.modalFormContent().inputs().filter(x -> x.getAttribute("name").equals("amount")).get(0).fill("25");
        page.modalFormContent().inputs().filter(x -> x.getAttribute("name").equals("payer.account")).get(0).fill(client.getAccounts().get(0).getAccount_number());
        if (page.modalFormContent().dropdown().dropMenuList().isDisplayed()) {
            page.dropdown().select(page.modalFormContent().inputs().filter(x -> x.getAttribute("name").equals("payer.account")).get(0).getValue());
        }

    }


    //TODO: Реализовать метод с входящими параметрами
    @И("заполняет поля плательщика при создании документа с параметрами")
    public void fillDocument(Map<String, String> params) {
    }

    @И("пользователь {string} c найденной организацией {string}")
    public void getUser(String inputUser, String stashOrg) {
        JSONObject userData = (JSONObject) Stash.getStashInstance().getStash(inputUser);
        Client client = (Client) Stash.getStashInstance().getStash(stashOrg);
        connectionUserClient(userData, client);
    }

    private void connectionUserClient(JSONObject userData, Client client) {
//        HashMap<String, String> entityIds = new HashMap<>();
//        entityIds.put("uid", userData.getString("userId"));
//        entityIds.put("clientID", client.getId());
//        if (userData.getJSONArray("orgId").toList().isEmpty() || !(userData.getJSONArray("orgId").toString().contains("\"id\": " + client.getId()))) {
//            String roleId = DataProviderClient.getRandomUser().getRoles().
//            createConnection(entityIds, roleId);
//        }
    }

//    private void createConnection(Map<String, String> inputParams, String roleId) {
//        getApiClient().getDataStringFromService("добавить пользователя к организации", inputParams, new JSONObject("{\"actorRoles\":[{\"roleId\":" + roleId + ",\"availabilityPeriods\":[]}]}"));
//    }

//    private String getRole(Client client) {
//        String role = "";
//        JSONArray clientRoles = new JSONArray(getApiClient().getDataStringFromService("получить роль организации", new HashMap<String, String>() {{
//            put("clientId", client.getId());
//        }}, null));
//        if (clientRoles.length() == 0) {
//            String templateId = String.valueOf(getApiClient().getDataListFromDP(Role.class, "получить роль по наименованию", new HashMap<String, String>() {{
//                put("templName", "%D0%A1%D0%BE%D1%82%D1%80%D1%83%D0%B4%D0%BD%D0%B8%D0%BA%20%D0%BA%D0%BB%D0%B8%D0%B5%D0%BD%D1%82%D0%B0");//name = сотрудник клиента
//            }}).get(0));
//            role = new ValidateRole().generate(new HashMap<String, String>() {{
//                put("clientId", client.getId());
//                put("templateId", templateId);
//                put("templateName", "Сотрудник клиента");
//                put("id", templateId);
//            }});
//        } else {
//            for (int i = clientRoles.length() - 1; i >= 0; i--) {
//                if (clientRoles.getJSONObject(i).getString("templateName").equals("Сотрудник клиента")) {
//                    role = String.valueOf(clientRoles.getJSONObject(i).getInt("id"));
//                }
//            }
//        }
//        return role;
//    }


    @И("случайный пользователь")
    public void getAnyUser() {
        ApiClient apiClient = new ApiClient();
        HashMap<String, String> params = new HashMap<>();
        User user = getRandomUser();
        String validatedUser = validateUser(user);
        params.put("uid", validatedUser);
//        User userData = DataProviderClient.getUserById(User.class, "получить пользователя по id", params);
//        Stash.getStashInstance().addStashObj("случайный пользователь", userData);
    }

    @И("открыта форма {string}")
    public void formIsOpen(String formName) {
        CommonPage page = getPageByTitle(formName);
        HeaderBlock headerBlock = page.getHeader();
        assertsManager.criticalAssert().assertTrue(headerBlock.isDisplayed(), "Элемент  не найден на странице");
        assertsManager.criticalAssert().assertEquals(headerBlock.getText(), formName);
    }

    @И("импортировать подготовленный файл")
    public void importFile() {
        ImportPage page = getPage(ImportPage.class);
        File fileToImport = (File) Stash.getStashInstance().getStash("сохраненный документ");
        page.uploadField().sendKeys(fileToImport.getAbsolutePath());
    }

    @И("подготовить файл {string} для импорта")
    public void prepateFileToImport(String type) throws IOException {
        Client payer = (Client) Stash.getStashInstance().getStash("плательщик");
        Client recipient = (Client) Stash.getStashInstance().getStash("получатель");

        switch (type) {
            case "платёжное поручение": {
                String tempFile = "1CClientBankExchange\n" +
                        "Версия Формата=1.02\n" +
                        "Кодировка=Windows\n" +
                        "Отправитель=Бухгалтерия предприятия, редакция 4.2\n" +
                        "Получатель=\n" +
                        "Дата создания=" + LocalDate.now().format(DateTimeFormatter.ofPattern("dd.MM.yyyy")) + "\n" +
                        "Время создания=" + LocalTime.now().format(DateTimeFormatter.ofPattern("HH:mm:ss")) + "\n" +
                        "Документ=Платежное поручение\n" +
                        "СекцияДокумент=Платежное поручение\n" +
                        "Номер=" + (long) (Math.random() * 99999) + "\n" +
                        "Дата=" + LocalDate.now().format(DateTimeFormatter.ofPattern("dd.MM.yyyy")) + "\n" +
                        "Сумма=15\n" +
                        "ПлательщикСчет=" + payer.getAccounts().get(0).getAccount_number() + "\n" +
                        "Плательщик=" + payer.getFull_name() + "\n" +
                        "Плательщик1=" + payer.getShort_name() + "\n" +
                        "ПлательщикИНН=" + payer.getInn() + "\n" +
                        "ПлательщикКПП=" + payer.getKpp() + "\n" +
                        "ПлательщикРасчСчет=" + payer.getAccounts().get(0).getAccount_number() + "\n" +
                        "ПлательщикБанк1=" + payer.getBranches().get(0).getFull_name() + "\n" +
                        "ПлательщикБанк2=" + payer.getBranches().get(0).getShort_name() + "\n" +
                        "ПлательщикБик=" + payer.getBranches().get(0).getHeadBranch().getBic() + "\n" +
                        "ПлательщикКорсчет=" + payer.getBranches().get(0).getCorrespondent_account() + "\n" +
                        "ПолучательСчет=" + recipient.getAccounts().get(0).getAccount_number() + "\n" +
                        "Получатель=" + recipient.getFull_name() + "\n" +
                        "Получатель1=" + recipient.getShort_name() + "\n" +
                        "ПолучательИНН=" + recipient.getInn() + "\n" +
                        "ПолучательКПП=" + recipient.getKpp() + "\n" +
                        "ПолучательРасчСчет=" + recipient.getAccounts().get(0).getAccount_number() + "\n" +
                        "ПолучательБанк1=" + recipient.getBranches().get(0).getFull_name() + "\n" +
                        "ПолучательБанк2=" + recipient.getBranches().get(0).getShort_name() + "\n" +
                        "ПолучательБик=" + recipient.getBranches().get(0).getHeadBranch().getBic() + "\n" +
                        "ПолучательКорсчет=" + recipient.getBranches().get(0).getCorrespondent_account() + "\n" +
                        "ВидОплаты=01\n" +
                        "ВидПлатежа=Срочно\n" +
                        "Код=0\n" +
                        "НазначениеПлатежа=Перевод в другую организацию. В том числе НДС 20% - 2.50\n" +
                        "Очередность=5\n" +
                        "КонецДокумента\n" +
                        "КонецФайла";
                File file = createFile(tempFile);
                Stash.getStashInstance().addStashObj("сохраненный документ", file);
            }
            case "что-то ещё": {
                //TODO реализовать для остальных документов
            }

            default: {

            }

        }
    }

    private File createFile(String template) throws IOException {
        String fileName = "autoTest_" + (int) (Math.random() * 1000) + ".txt";
        new File("target" + System.getProperty("file.separator") + "fileForImport").mkdir();
        Path createdFile = Files.createFile(Paths.get("target", "fileForImport", fileName));
        return Files.write(createdFile, template.getBytes()).toFile();
    }

}