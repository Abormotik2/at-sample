package steps.sdas.json;

import cucumber.api.java.ru.Если;
import org.json.JSONArray;
import steps.BaseSteps;
import utils.DateUtils;
import utils.JsonUtils;

import java.time.LocalDateTime;
import java.util.List;

import static utils.CollectionUtils.getRandom;
import static utils.DateUtils.localDateTimeToString;

public class JsonHelperStep extends BaseSteps {

    @Если("сохраняю содержимое поля {string} документа {string} в переменную {string}")
    public void getFieldValueFromJSONObject(String fieldPath, String jsonKey, String fieldValueKey) {
        String json = getCitrusVariable(jsonKey);
        String value = JsonUtils.getFieldValueByPathFromJson(fieldPath, json);
        setCitrusVariable(fieldValueKey, value);
    }

    @Если("сохраняю список dataTime полей {string} списка документов {string} в переменную {string}")
    public void getLocalDateTimeListFromJSONArray(String fieldPath, String jsonArrayKey, String localDateTimeListKey) {
        JSONArray jsonArray = getCitrusVariable(jsonArrayKey, JSONArray.class);
        List<LocalDateTime> localDateTimeList = JsonUtils.getLocalDateTimeListFromJSONArray(jsonArray, fieldPath);
        setCitrusVariable(localDateTimeListKey, localDateTimeList);
    }

    @Если("сохранияю список String полей {string} списка документов {string} в переменную {string}")
    public void getStringListFromJSONArray(String fieldPath, String jsonArrayKey, String localDateTimeListKey) {
        JSONArray jsonArray = getCitrusVariable(jsonArrayKey, JSONArray.class);
        List<String> stringList = JsonUtils.getStringListFromJSONArray(jsonArray, fieldPath);
        setCitrusVariable(localDateTimeListKey, stringList);
    }

    @Если("сохраняю случайное значение поля даты {string} списка документов {string} в переменную {string}")
    public void getRandomDateFromJSONArray(String fieldPath, String jsonArrayKey, String randomDateKey) {
        JSONArray jsonArray = getCitrusVariable(jsonArrayKey, JSONArray.class);
        List<LocalDateTime> localDateTimeList = JsonUtils.getLocalDateTimeListFromJSONArray(jsonArray, fieldPath);
        String randomDate = DateUtils.localDateTimeToString(getRandom(localDateTimeList), "yyyy-MM-dd");
        setCitrusVariable(randomDateKey, randomDate);
    }

}
