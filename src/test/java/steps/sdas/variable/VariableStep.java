package steps.sdas.variable;

import cucumber.api.java.ru.Если;
import steps.BaseSteps;

public class VariableStep extends BaseSteps {

    @Если("сохраняю строку {string} в переменную {string}")
    public void getFieldValueFromJSONObject(String value, String key) {
        setCitrusVariable(key, value);
    }

}
