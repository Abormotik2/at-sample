package steps.sdas.data;


import cucumber.api.java.ru.Если;
import org.apache.commons.lang3.RandomUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import steps.BaseSteps;
import utils.LambdaUtils;

import java.time.LocalDate;

import static org.apache.commons.lang3.RandomStringUtils.randomNumeric;
import static utils.DateUtils.getRandomDate;
import static utils.DateUtils.localDateTimeToString;

public class CreteArchivalDocumentStep extends BaseSteps {

    @Если("сформирован json документа для сохранения в СДАС и сохранен в {string}")
    public void setCitrusArchivalDocument(String createdDocKey) {
        setCitrusVariable(createdDocKey, createArchivalDocument(true));
    }

    @Если("сформирован json документа для сохранения в СДАС c типом документа {string} и сохранен в {string}")
    public void setCitrusArchivalDocument(String docTypeKey, String createdDocKey) {
        JSONObject docType = getCitrusJson(docTypeKey);
        JSONObject doc = createArchivalDocument(true);
        doc
                .put("docTypeExtId", docType.getString("docTypeExtId"))
                .put("docTypeName", docType.getString("docTypeName"))
                .put("extSystemId", docType.getString("extSystemId"))
                .put("extTableName", docType.getString("extTableName"));
        setCitrusVariable(createdDocKey, doc);
    }

    @Если("сформирован список json документов с прикрепленными документами и без для сохранения в СДАС и сохранен в {string}")
    public void setCitrusArchivalDocumentListWithAndWithoutAttach(String createdDocKey) {
        JSONArray jsonArray = new JSONArray();
        jsonArray.put(createArchivalDocument(true));
        jsonArray.put(createArchivalDocument(false));
        setCitrusVariable(createdDocKey, jsonArray);
    }

    @Если("сформирован список json документов для сохранения в СДАС в кол-ве {string} шт и сохранен в {string}")
    public void setCitrusArchivalDocumentList(String count, String createdDocKey) {
        JSONArray jsonArray = new JSONArray();
        LambdaUtils.repeat(Integer.parseInt(count), () -> jsonArray.put(createArchivalDocument(true)));
        setCitrusVariable(createdDocKey, jsonArray);
    }

    @Если("сформирован json типа документа для сохранения в СДАС и сохранен в {string}")
    public void setCitrusArchivalDocumentType(String createdDocKey) {
        setCitrusVariable(createdDocKey, createArchivalDocumentType());
    }

    @Если("сформирован json ожидаемых активных действий клмента и сохранен в {string}")
    public void setCitrusActiveActionList(String createdDocKey) {
        setCitrusVariable(createdDocKey,
                "[\n" +
                        "  {\n" +
                        "    \"name\": \"LEGACY_DBO_DOCUMENTS_VIEW\",\n" +
                        "    \"description\": \"Документы архивных систем (Просмотреть)\"\n" +
                        "  },\n" +
                        "  {\n" +
                        "    \"name\": \"LEGACY_DBO_DOCUMENTS_PRINT\",\n" +
                        "    \"description\": \"Документы архивных систем (Печатать)\"\n" +
                        "  }\n" +
                        "]");
    }

    private JSONObject createArchivalDocument(boolean withAttachment) {
        String attachments = withAttachment? createAttachments(1).toString(1) : "[]";
        String documentNumber = randomNumeric(6);
        String documentDate = localDateTimeToString(getRandomDate(LocalDate.now().minusYears(1), LocalDate.now()).atStartOfDay());
        String payerAccount = "407028107" + randomNumeric(11);
        String receiverAccount = "407028107" + randomNumeric(11);
        String receiverCorrAccount = "407028107" + randomNumeric(11);
        int docTypeExtId = RandomUtils.nextInt();
        String docTypeName = "docTypeNameForAutotest(" + randomNumeric(5) + ")";
        int extSystemId = RandomUtils.nextInt();
        String extTableName = "extTableNameForAutotest(" + randomNumeric(5) + ")";
        String receiverBic = randomNumeric(9);
        String jsonNewDock = "{\n" +
                "  \"attachments\" : " + attachments + ",\n" +
                "  \"documentNumber\" : \"" + documentNumber + "\",\n" +
                "  \"documentDate\" : \"" + documentDate + "\",\n" +
                "  \"docTypeExtId\" : \"" + docTypeExtId + "\",\n" +
                "  \"docTypeName\" : \"" + docTypeName + "\",\n" +
                "  \"extSystemId\" : \"" + extSystemId + "\",\n" +
                "  \"extTableName\" : \"" + extTableName + "\",\n" +
                "  \"orgName\" : \"АО \\\"Торговый дом \\\"ПЕРЕКРЕСТОК\\\"\",\n" +
                "  \"branchName\" : \"ФИЛИАЛ БАНКА ВТБ (ПАО) В Г. РОСТОВЕ-НА-ДОНУ\",\n" +
                "  \"sourceName\" : \"ДБО 3.17\",\n" +
                "  \"signer\" : \"Тестов Автотест\",\n" +
                "  \"content\" : \"Контент документа для автотеста(" + randomNumeric(5) + ")\",\n" +
                "  \"status\" : {\n" +
                "    \"base\" : \"EXECUTED\",\n" +
                "    \"extended\" : \"\",\n" +
                "    \"comment\" : \"Исполнен\"\n" +
                "  },\n" +
                "  \"clientId\" : 1,\n" +
                "  \"payerAccount\" : \"" + payerAccount + "\",\n" +
                "  \"receiverAccount\" : \"" + receiverAccount + "\",\n" +
                "  \"receiverBic\" : \"" + receiverBic + "\",\n" +
                "  \"receiverCorrAccount\" : \"" + receiverCorrAccount + "\",\n" +
                "  \"externalId\" : \"1\",\n" +
                "}";
        return new JSONObject(jsonNewDock);
    }

    private JSONArray createAttachments(int count) {
        int attachmentId = RandomUtils.nextInt();
        String creationTime = localDateTimeToString(getRandomDate(LocalDate.now().minusYears(2), LocalDate.now()).atStartOfDay());;
        String extension = "txt";
        String fileName = "fileForAutotest_" + randomNumeric(6);
        String hash = randomNumeric(10);
        int size = RandomUtils.nextInt();
        int typeExtId = RandomUtils.nextInt();

        JSONArray jsonArray = new JSONArray();
        LambdaUtils.repeat(count, () -> {
            String json = "" +
                    "{\n" +
                    "      \"attachmentId\": " + attachmentId + ",\n" +
                    "      \"creationTime\": \"" + creationTime + "\",\n" +
                    "      \"extension\": \"" + extension + "\",\n" +
                    "      \"fileName\": \"" + fileName + "\",\n" +
                    "      \"hash\": \"" + hash + "\",\n" +
                    "      \"size\": " + size + ",\n" +
                    "      \"typeExtId\": " + typeExtId + "\n" +
                    "    }";
            jsonArray.put(new JSONObject(json));
        });
        return jsonArray;
    }

    private JSONObject createArchivalDocumentType() {
        int docTypeExtId = RandomUtils.nextInt();
        String docTypeName = "docTypeNameForAutotest(" + randomNumeric(5) + ")";
        int extSystemId = RandomUtils.nextInt();
        String extTableName = "extTableNameForAutotest(" + randomNumeric(5) + ")";
        String jsonNewDockType = "{\n" +
                "  \"docTypeExtId\": \"" + docTypeExtId + "\",\n" +
                "  \"docTypeName\": \"" + docTypeName + "\",\n" +
                "  \"extSystemId\": \"" + extSystemId + "\",\n" +
                "  \"extTableName\": \"" + extTableName + "\",\n" +
                "}";
        return new JSONObject(jsonNewDockType);
    }
}
