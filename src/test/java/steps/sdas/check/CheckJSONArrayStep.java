package steps.sdas.check;

import com.google.common.collect.Ordering;
import cucumber.api.java.ru.Тогда;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.json.JSONArray;
import org.json.JSONObject;
import org.unitils.reflectionassert.ReflectionAssert;
import steps.BaseSteps;
import utils.DateUtils;
import utils.JsonUtils;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.unitils.reflectionassert.ReflectionComparatorMode.LENIENT_ORDER;

public class CheckJSONArrayStep extends BaseSteps {

    @Тогда("актуальный список json {string} соответствует ожидаемому списку json {string} без учета порядка")
    public void compareJSONArrayWithoutOrder(String expectedJsonArrayKey, String actualJsonArrayKey) {
        List<JSONObject> expectedJsonArray = JsonUtils.jsonArrayToJSONObjectList(
                new JSONArray(getCitrusVariable(expectedJsonArrayKey)));
        List<JSONObject> actualJsonArray = JsonUtils.jsonArrayToJSONObjectList(
                new JSONArray(getCitrusVariable(actualJsonArrayKey)));
        String errMsg = String.format("Ошибка! JsonArray %s не равен JsonArray %s без учета порядка", actualJsonArrayKey, expectedJsonArrayKey);
        ReflectionAssert.assertReflectionEquals(errMsg, expectedJsonArray, actualJsonArray, LENIENT_ORDER);
    }

    @Тогда("поля {string} списка json {string} содержат {string}")
    public void checkStepJSONArrayContainsFieldsContainsCitrusValue(
            String actualFieldPath, String actualJSONArrayKey, String expectedStringKey) {
        checkJSONArrayFieldsByMatcher(actualJSONArrayKey, actualFieldPath, expectedStringKey, Matchers::containsString);
    }

    @Тогда("поля {string} списка json {string} не равны {string}")
    public void checkStepJSONArrayContainsFieldsNotEqualToCitrusValue(
            String actualFieldPath, String actualJSONArrayKey, String expectedStringKey) {
        Function<String, Matcher<String>> matcher = (s) -> Matchers.not(Matchers.equalTo(s));
        checkJSONArrayFieldsByMatcher(actualJSONArrayKey, actualFieldPath, expectedStringKey, matcher);
    }

    @Тогда("поля {string} списка json {string} равны {string}")
    public void checkStepJSONArrayContainsFieldsEqualToCitrusValue(
            String actualFieldPath, String actualJSONArrayKey, String expectedStringKey) {
        Function<String, Matcher<String>> matcher = Matchers::equalTo;
        checkJSONArrayFieldsByMatcher(actualJSONArrayKey, actualFieldPath, expectedStringKey, Matchers::equalTo);
    }

    @Тогда("даты LocalDataTime списка {string} больше или равны даты String {string}")
    public void checkLocalDateTimeListGreaterThanOrEqual(String localDataTimeListKey, String stringDateKey) {
        List<LocalDateTime> localDataTimeList = getCitrusList(localDataTimeListKey);
        String localDateTimeStr = getCitrusVariable(stringDateKey);
        LocalDateTime localDateTime = DateUtils.stringToLocalDate(localDateTimeStr).atStartOfDay();
        localDataTimeList.forEach(actualDateTime -> {
            String errMsg = String.format("Ошибка дата %s, мешьше даты № %s", actualDateTime, localDateTimeStr);
            assertThat(errMsg, actualDateTime.isAfter(localDateTime) || actualDateTime.isEqual(localDateTime));
        });
    }

    @Тогда("даты LocalDataTime списка {string} меньше или равны даты String {string}")
    public void checkLocalDateTimeListLessThanOrEqual(String localDataTimeListKey, String stringDateKey) {
        List<LocalDateTime> localDataTimeList = getCitrusList(localDataTimeListKey);
        String localDateTimeStr = getCitrusVariable(stringDateKey);
        LocalDateTime localDateTime = DateUtils.stringToLocalDate(localDateTimeStr).atStartOfDay();
        localDataTimeList.forEach(actualDateTime -> {
            String errMsg = String.format("Ошибка дата %s, больше даты № %s", actualDateTime, localDateTimeStr);
            assertThat(errMsg, actualDateTime.isBefore(localDateTime) || actualDateTime.isEqual(localDateTime));
        });
    }

    private void checkJSONArrayFieldsByMatcher(String actualJSONArrayKey, String actualFieldPath, String expectedStringKey, Function<String, Matcher<String>> matcher) {
        JSONArray actualJSONArray = new JSONArray(getCitrusVariable(actualJSONArrayKey));
        String expectedString = getCitrusVariable(expectedStringKey);
        int size = actualJSONArray.length();
        for (int i = 0; i < actualJSONArray.length(); i++) {
            JSONObject jsonObject = actualJSONArray.getJSONObject(i);
            String actualFieldValue = JsonUtils.getFieldValueByPathFromJson(actualFieldPath, jsonObject.toString());
            String errMsg = String.format("Ошибка при проверки поля списка %s (размером %d), объекта № %d, поля %s", actualJSONArrayKey, size, i, actualFieldPath);
            assertThat(errMsg, actualFieldValue, matcher.apply(expectedString));
        }
    }
}
