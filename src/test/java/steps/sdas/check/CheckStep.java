package steps.sdas.check;

import com.google.common.collect.Ordering;
import cucumber.api.java.ru.Тогда;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.json.JSONArray;
import org.json.JSONObject;
import org.unitils.reflectionassert.ReflectionAssert;
import org.unitils.reflectionassert.ReflectionComparatorMode;
import steps.BaseSteps;
import utils.DateUtils;
import utils.JsonUtils;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.unitils.reflectionassert.ReflectionComparatorMode.LENIENT_ORDER;

public class CheckStep extends BaseSteps {

    @Тогда("поля json {string} соответстуют полям json {string}")
    public void compareObjectsByFields(String expectedJsonKey, String actualJsonKey, List<String> fieldPathList) {
        JSONObject actualJson = new JSONObject(getCitrusVariable(actualJsonKey));
        JSONObject expectedJson = new JSONObject(getCitrusVariable(expectedJsonKey));
        fieldPathList.forEach(fieldPath -> {
            assertThat(
                    String.format("Ошибка! Поля %s json %s и %s не равны", fieldPath, actualJsonKey, expectedJsonKey),
                    JsonUtils.getFieldValueByPathFromJson(fieldPath, actualJson.toString(1)),
                    equalTo(JsonUtils.getFieldValueByPathFromJson(fieldPath, expectedJson.toString(1))));
        });
    }

    @Тогда("поля json {string} соответстуют указанным строкам:")
    public void compareFieldsJsonAndValue(String expectedJsonKey, Map<String, String> fieldPathValueMap) {
        String expectedJson = getCitrusVariable(expectedJsonKey);
        fieldPathValueMap.forEach((fieldPath, value) -> {
            String errMsg = String.format("Ошибка! Поле %s json %s не равно ожидаемому значению", fieldPath, expectedJsonKey);
            assertThat(errMsg, JsonUtils.getFieldValueByPathFromJson(fieldPath, expectedJson), equalTo(value));
        });
    }

    @Тогда("список json {string} не пустой")
    public void checkJSONArrayIsNotEmpty(String actualJSONArrayKey) {
        JSONArray jsonArray = new JSONArray(getCitrusVariable(actualJSONArrayKey));
        assertThat("Ошибка! Json " + actualJSONArrayKey + " пустой", jsonArray.length(), Matchers.greaterThan(0));
    }

    @Тогда("json {string} пустой")
    public void checkJsonIsEmpty(String jsonKey) {
        String json = getCitrusVariable(jsonKey);
        String errMsg = String.format("Ошибка! Json %s не пустой\n%s: %s", jsonKey, jsonKey, json);
        assertThat(errMsg, json.isEmpty());
    }

    @Тогда("проверяю сортировку списка строк {string} в порядке возрастания")
    public void checkSortedStringAsc(String localDataTimeListKey) {
        List<String> stringList = getCitrusList(localDataTimeListKey);
        Ordering.natural().nullsLast().isOrdered(stringList);
        String errMsg = "Ошибка! Список НЕ отсортирован в порядке возрастания:" + stringList;
        assertThat(errMsg, Ordering.natural().nullsLast().isOrdered(stringList));
    }

    @Тогда("проверяю сортировку списка строк {string} в порядке убывания")
    public void checkSortedStringDesc(String localDataTimeListKey) {
        List<String> stringList = getCitrusList(localDataTimeListKey);
        Ordering.natural().nullsLast().isOrdered(stringList);
        String errMsg = "Ошибка! Список НЕ отсортирован в порядке убывания:" + stringList;
        assertThat(errMsg, Ordering.natural().nullsLast().reverse().isOrdered(stringList));
    }
}
