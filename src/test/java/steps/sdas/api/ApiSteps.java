package steps.sdas.api;


import com.consol.citrus.message.Message;
import cucumber.api.java.ru.Если;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import ru.vtb.at.datavalidator.Config;
import steps.BaseSteps;
import utils.ApiClient;
import utils.JsonUtils;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static steps.sdas.api.ApiSteps.UserType.CLIENT;


public class ApiSteps extends BaseSteps {

    private ApiClient api = new ApiClient();

    @Если("клиентом отправить запрос {string}, сохранить data ответа в {string}, queryParam:")
    public void sendRequestSaveDataWithQueryParam(String requestName, String responseKey, Map<String, String> paramKeys) {
        String response = sendRequestMain(CLIENT.type, requestName, null, null, paramKeys);
        JSONArray data = new JSONObject(response).getJSONArray("data");
        setCitrusVariable(responseKey, data);
    }

    @Если("я с ролью {string} отправляю запрос {string} и сохраняю содержимое {string} ответа в {string}, параметры запроса - queryParam:")
    public void sendRequestSaveDataWithQueryParam(String userType, String requestName, String contentType, String responseKey, Map<String, String> paramKeys) {
        String response = sendRequestMain(userType, requestName, null, null, paramKeys);
        setCitrusVariable(responseKey, getContentFromResponse(contentType, response));
    }

    @Если("я с ролью {string} отправляю запрос {string} и сохраняю содержимое {string} ответа в {string}, параметры запроса - pathParam:")
    public void sendRequestSaveDataWithPathParam(String userType, String requestName, String contentType, String responseKey, Map<String, String> paramKeys) {
        String response = sendRequestMain(userType, requestName, null, paramKeys, null);
        setCitrusVariable(responseKey, getContentFromResponse(contentType, response));
    }

    @Если("я с ролью {string} отправляю запрос {string} и сохраняю содержимое {string} ответа в {string}, параметры запроса - body: {string}, pathParam:")
    public void sendRequestSaveDataWithPathParam(String userType, String requestName, String contentType, String responseKey, String bodyKey, Map<String, String> paramKeys) {
        String response = sendRequestMain(userType, requestName, bodyKey, paramKeys, null);
        setCitrusVariable(responseKey, getContentFromResponse(contentType, response));
    }

    @Если("я с ролью {string} отправляю запрос {string} и сохраняю содержимое {string} ответа в {string}, параметры запроса - body: {string}")
    public void sendRequestSaveDataWithQueryParam(String userType, String requestName, String contentType, String responseKey, String bodyKey) {
        String response = sendRequestMain(userType, requestName, bodyKey, null, null);
        setCitrusVariable(responseKey, getContentFromResponse(contentType, response));
    }

    private String sendRequestMain(String userType, String requestName, String bodyKey, Map<String, String> pathParamsKeys, Map<String, String> queryParamsKeys) {
        String body = bodyKey != null ? getCitrusVariable(bodyKey) : null;
        Map<String, String> pathParams = getParamsFromCitrus(pathParamsKeys);
        Map<String, String> queryParams = getParamsFromCitrus(queryParamsKeys);
        String token = getToken(UserType.getUserTypeByType(userType));
        Message response = api.getDataStringFromService(requestName, new HashMap<>(), pathParams, queryParams, body, token);
        return response.getPayload().toString();
    }

    private Map<String, String> getParamsFromCitrus(Map<String, String> paramKeys) {
        if (paramKeys == null) {
            return new HashMap<>();
        }
        Map<String, String> param = new HashMap<>();
        paramKeys.forEach((key, valueMap) -> {
            String value;
            if (valueMap.matches(".*\\$\\{.*}.*")) {
                String citrusKey = StringUtils.substringBetween(valueMap, "${", "}");
                String citrusVar = getCitrusVariable(citrusKey);
                value = valueMap.replace("${" + citrusKey + "}", citrusVar);
            } else {
                value = getCitrusVariable(valueMap);
            }
            String encodeValue;
            try {
                encodeValue = URLEncoder.encode(value, StandardCharsets.UTF_8.toString());
            } catch (UnsupportedEncodingException e) {
                String errMessage = String.format("Не удалось закодировать параметр &%s=%s для Rest запроса", key, value);
                throw new RuntimeException(errMessage, e);
            }
            param.put(key, encodeValue);
        });
        return param;
    }

    private String getToken(UserType userType) {
        switch (userType) {
            case CLIENT:
                return getApiClient().getClientToken();
            case ADMIN:
                return Config.getStringSystemProperty("admin.token");
            default:
                return null;
        }
    }

    private String getContentFromResponse(String contentType, String response) {
        String prefix = "body.";
        if (contentType.equals("body")) {
            return response;
        } else if (StringUtils.startsWith(contentType, prefix)) {
            String path = contentType.replaceFirst("body\\.", "");
            return JsonUtils.getFieldValueByPathFromJson(path, response);
        } else {
            throw new IllegalArgumentException("Неизвестный тип содержимого ответа: " + contentType);
        }
    }



    enum UserType {
        CLIENT("сотрудник организации"),
        ADMIN("администратор");

        private String type;

        UserType(String type) {
            this.type = type;
        }

        public static UserType getUserTypeByType(String userType) {
            return Arrays.asList(UserType.values()).stream()
                    .filter(type -> type.type.equals(userType))
                    .findAny()
                    .orElseThrow(() -> new IllegalArgumentException("Отсутсвует тип пользователя: " + userType));
        }

        public String getType() {
            return type;
        }
    }
}