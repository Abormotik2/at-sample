package steps;


import assertion.AssertsManager;

import org.json.JSONObject;
import ru.vtb.at.steps.AbstractFrameworkSteps;
import utils.ApiClient;

import java.util.List;


public abstract class BaseSteps extends AbstractFrameworkSteps {
    protected AssertsManager assertsManager = AssertsManager.getAssertsManager();

    public ApiClient getApiClient() {
        return new ApiClient();
    }

    protected JSONObject getCitrusJson(String key) {
        return super.getCitrusVariable(key, JSONObject.class);
    }

    protected List getCitrusList(String key) {
        return super.getCitrusVariable(key, List.class);
    }

}
