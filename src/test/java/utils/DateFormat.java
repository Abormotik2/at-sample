package utils;



/**
 * Часто используемые форматы даты и времени
 */

public enum DateFormat {
    DEFAULT_DATETIME_FORMAT("yyyy-MM-dd'T'HH:mm:ss'Z'"),
    DEFAULT_DATE_FORMAT("yyyy-MM-dd");

    DateFormat(String format) {
        this.format = format;
    }

    private final String format;

    public String getFormat() {
        return format;
    }
}
