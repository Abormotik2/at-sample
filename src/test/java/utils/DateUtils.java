package utils;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoField;
import java.util.Random;

public abstract class DateUtils {

    /**
     * Получить случайную дату в заданном диапазоне
     *
     * @param startDate начало периода
     * @param endDate   конец периода
     * @return случайная дата в пределах заданных диапазонах
     */
    public static LocalDate getRandomDate(LocalDate startDate, LocalDate endDate) {
        long startDay = startDate.getLong(ChronoField.EPOCH_DAY);
        long endDay = endDate.getLong(ChronoField.EPOCH_DAY);
        int randomDay = new Random().nextInt((int) (endDay - startDay) + 1);
        return Instant.ofEpochSecond((startDay + randomDay) * 24 * 60 * 60).atZone(ZoneId.systemDefault()).toLocalDate();
    }

    /**
     * Преобразовать LocalDateTime в String по формату
     *
     * @param localDateTime дата с временем
     * @param format формат преобразования
     * @return String в указанном формате
     */
    public static String localDateTimeToString(LocalDateTime localDateTime, String format){
        return localDateTime.format(DateTimeFormatter.ofPattern(format));
    }

    /**
     * Преобразовать String в localDateTime в стандартном формате сервиса
     *
     * @param localDateTime дата с временем
     * @return String в стандартном формате сервиса
     */
    public static String localDateTimeToString(LocalDateTime localDateTime){
        return localDateTime.format(DateTimeFormatter.ofPattern(DateFormat.DEFAULT_DATETIME_FORMAT.getFormat()));
    }

    /**
     * Преобразовать String в LocalDateTime по формату
     *
     * @param dateTimeStr дата в формате String
     * @param format формат преобразования
     * @return LocalDateTime преобразованная из String по формату
     */
    public static LocalDateTime stringToDateTime(String dateTimeStr, String format){
        return LocalDateTime.parse(dateTimeStr, DateTimeFormatter.ofPattern(format));
    }

    /**
     * Преобразовать String в LocalDateTime в стандартном формате сервиса
     *
     * @param dateTimeStr дата в формате String в стандартном формате сервиса
     * @return LocalDateTime преобразованная из String в стандартном формате сервиса
     */
    public static LocalDateTime stringToDateTime(String dateTimeStr){
        return LocalDateTime.parse(dateTimeStr, DateTimeFormatter.ofPattern(DateFormat.DEFAULT_DATETIME_FORMAT.getFormat()));
    }

    /**
     * Преобразовать String в LocalDate по формату
     *
     * @param dateStr дата в формате String
     * @param format формат преобразования
     * @return LocalDate преобразованная из String по формату
     */
    public static LocalDate stringToLocalDate(String dateStr, String format){
        return LocalDate.parse(dateStr, DateTimeFormatter.ofPattern(format));
    }

    /**
     * Преобразовать String в LocalDate в стандартном формате сервиса
     *
     * @param dateStr дата в формате String в стандартном формате сервиса
     * @return LocalDate преобразованная из String в стандартном формате сервиса
     */
    public static LocalDate stringToLocalDate(String dateStr){
        return stringToLocalDate(dateStr, DateFormat.DEFAULT_DATE_FORMAT.getFormat());
    }
}
