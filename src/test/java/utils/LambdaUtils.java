package utils;

import java.util.stream.IntStream;

public abstract class LambdaUtils {

    /**
     * Повторить выполнение переданной функции
     * @param count кол-во повторений
     * @param run функция без аргументов и возвращаемых значений которую необходимо повторять
     */
    public static void repeat(int count, Runnable run) {
        IntStream.range(0, count).forEach(s -> run.run());
    }
}
