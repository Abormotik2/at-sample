package utils.csp;

import com.consol.citrus.dsl.endpoint.CitrusEndpoints;
import com.consol.citrus.http.client.HttpClient;
import com.consol.citrus.message.MessageType;
import com.google.common.io.Files;
import org.apache.http.entity.ContentType;
import ru.vtb.at.Config;
import ru.vtb.at.citrus.CitrusRunner;
import ru.vtb.at.context.Context;
import ru.vtb.at.exceptions.FrameworkRuntimeException;

import java.io.*;
import java.util.LinkedHashMap;
import java.util.Map;

public class CSPRootCert {

    private static Map<String, LinkedHashMap<String, String>> vals = new CSPConfigUtils().readConfig();
    private static final File tmp = Files.createTempDir();
    private static final String ROOT_CA_PATH = tmp + File.separator + "root_ca.cer";

    public static boolean isRootCertPresent() throws IOException {
        Process process = new ProcessBuilder(vals.get("paths").get("certmgr"),
                "-list",
                "-store",
                "uRoot",
                "-thumbprint",
                vals.get("RootCA").get("hash"),
                "-dn",
                "CN=" + vals.get("RootCA").get("CN")
        ).start();
        String line;
        InputStream stream = process.getInputStream();
        BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
        while ((line = reader.readLine()) != null) {
            if (line.contains(vals.get("RootCA").get("hash"))) return true;
        }
        process.destroy();
        return false;
    }

    public static void getRootCAFromDP() {
        HttpClient client = getDPEndpoint();
        Context.getInstance().getBean(CitrusRunner.class).http(httpActionBuilder -> httpActionBuilder
                .client(client)
                .send()
                .get("/cert_root_ca?root_ca_name=eq." + vals.get("RootCA").get("CN") + "&select=root_ca")
                .header("Accept", "application/octet-stream")
                .accept(ContentType.APPLICATION_OCTET_STREAM.getMimeType())
        );

        Context.getInstance().getBean(CitrusRunner.class).http(httpActionBuilder -> httpActionBuilder
                .client(client)
                .receive()
                .response()
                .messageType(MessageType.BINARY)
                .extractFromPayload("$", "cert")
        );
        String content = Context.getInstance().getBean(CitrusRunner.class).getTestContext().getVariable("cert");
        CSPFileUtils.saveBinaryToFile(content, ROOT_CA_PATH);
    }

    public static void installRootCert() throws IOException {
        Process process = new ProcessBuilder(vals.get("paths").get("certmgr"),
                "-inst",
                "-store",
                "uRoot",
                "-file",
                ROOT_CA_PATH
        ).start();
        String line;
        InputStream stream = process.getInputStream();
        BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
        while ((line = reader.readLine()) != null) {
            if (line.contains("An error occurred"))
                throw new FrameworkRuntimeException("Не удалось установить корневой сертификат");
        }
    }

    private static HttpClient getDPEndpoint() {
        return CitrusEndpoints
                .http()
                .client()
                .requestUrl(Config.getStringSystemProperty("dp.url"))
                .build();
    }
}
