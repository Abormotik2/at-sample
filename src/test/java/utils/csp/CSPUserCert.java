package utils.csp;

import com.google.common.io.Files;
import ru.vtb.at.businessmodel.certificate.BrowserCertificate;
import ru.vtb.at.exceptions.FrameworkRuntimeException;


import java.io.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * todo вынести в отдельную библиотеку VTBDBOAUTO-225
 */
public class CSPUserCert {

    private static Map<String, LinkedHashMap<String, String>> vals = new CSPConfigUtils().readConfig();
    private static final File tmp = Files.createTempDir();
    private static final String CERT_PATH = vals.get("paths").get("private_storage") + File.separator + "user.cer";
    private static final String PRIVATE_PATH = vals.get("paths").get("private_storage") + File.separator + "private.zip";

    public static boolean isCertificateInstalled(String certificateName) throws IOException {
        return getCertificates().contains(certificateName);
    }

    public static boolean isContainerExists(String containerName) throws IOException {
        return getContainers().contains(containerName);
    }

    private static List<String> getCertificates() throws IOException {
        List<String> containers = new ArrayList<>();
        Process process = new ProcessBuilder(vals.get("paths").get("certmgr"),
                "-list",
                "-store",
                "uMy"
        ).start();
        InputStream stream = process.getInputStream();
        String line;
        BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
        Pattern p = Pattern.compile("CN=(\\S+)$");
        while ((line = reader.readLine()) != null) {
            Matcher m = p.matcher(line);
            if (m.find()) {
                containers.add(m.group(1));
            }
        }
        process.destroy();
        return containers;
    }

    private static List<String> getContainers() throws IOException {
        List<String> containers = new ArrayList<>();
        Process process = new ProcessBuilder(String.format("\"%s\"", vals.get("paths").get("csptest")),
                "-keyset",
                "-enum_cont",
                "-verifycontext",
                "-fqcn"
        ).start();
        InputStream stream = process.getInputStream();
        String line;
        BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
        Pattern p = Pattern.compile("^(\\\\\\\\\\..+)$");
        while ((line = reader.readLine()) != null) {
            Matcher m = p.matcher(line);
            if (m.find()) {
                containers.add(m.group(1));
            }
        }
        process.destroy();
        return containers;
    }

    public static void getUserCertFromDP(BrowserCertificate certTemplate) {
        byte[] data = String.format("%s", certTemplate.getPublic_key()).getBytes();
        new File(vals.get("paths").get("private_storage")).mkdirs();
        CSPFileUtils.saveBinaryToFile(data, CERT_PATH);
    }

    public static void getUserPrivateKeyFromDP(BrowserCertificate certTemplate) throws IOException {
        String location = vals.get("paths").get("private_storage") + File.separator + extractNumbers(certTemplate.getCert_name());
        byte[] buffer = new byte[1024];
        ZipInputStream zin = new ZipInputStream(new ByteArrayInputStream(Base64.getDecoder().decode(certTemplate.getPrivate_key())));
        ZipEntry ze;
        File certLocation = new File(location);
        if (!certLocation.exists() && !certLocation.mkdir()) {
            throw new IOException("Не удалось создать директории");
        }
        while ((ze = zin.getNextEntry()) != null) {
            FileOutputStream fos = new FileOutputStream(location + File.separator + ze.getName());
            int len;
            while ((len = zin.read(buffer)) > 0) {
                fos.write(buffer, 0, len);
            }
            zin.closeEntry();
            fos.close();
        }
        zin.close();
    }

    public static void installUserCert(BrowserCertificate certTemplate) throws IOException {
        Process processSubst = new ProcessBuilder("subst",
                vals.get("paths").get("storage"),
                vals.get("paths").get("private_storage")
        ).start();
        String lineSubst;
        InputStream streamSubst = processSubst.getInputStream();
        BufferedReader readerSubst = new BufferedReader(new InputStreamReader(streamSubst));
        while ((lineSubst = readerSubst.readLine()) != null) {
            System.out.println(lineSubst);
        }
        Process process = new ProcessBuilder(String.format("\"%s\"", vals.get("paths").get("certmgr")),
                "-inst",
                "-store",
                "uMy",
                "-file",
                CERT_PATH,
                "-cont",
                getContainers().stream().filter(s -> s.contains(extractNumbers(certTemplate.getCert_name()))).findFirst().orElseThrow(FrameworkRuntimeException::new)
        ).start();
        String line;
        InputStream stream = process.getInputStream();
        BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
        while ((line = reader.readLine()) != null) {
            System.out.println(line);
            if (line.contains("An error occurred"))
                throw new FrameworkRuntimeException("Не удалось установить корневой сертификат");
        }
    }

    private static String extractNumbers(String certificateName) {
        Matcher m = Pattern.compile("(\\d+).").matcher(certificateName);
        if (m.find()) {
            return m.group(1);
        } else throw new FrameworkRuntimeException("Не удалось получить id контейнера закрытого ключа");
    }
}
