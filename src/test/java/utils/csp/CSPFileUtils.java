package utils.csp;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Base64;
import java.util.zip.ZipEntry;

public class CSPFileUtils {
    static void saveBinaryToFile(String content, String path) {
        String[] contentArr = content.replaceAll("\\[", "").replaceAll("\\]", "").split(",");
        OutputStream os = null;
        try {
            os = new FileOutputStream(path);
            for (String val : contentArr) {
                int res = Integer.parseInt(String.format("%02X", Byte.parseByte(val)), 16);
                os.write((byte) res);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                os.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    static void saveBinaryToFile(byte[] content, String path) {
        OutputStream os = null;
        try {
            os = new FileOutputStream(path);
            os.write(String.format("-----BEGIN CERTIFICATE-----\n%s\n-----END CERTIFICATE-----", new String(content)).getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                os.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    public static File newFile(File destinationDir, ZipEntry zipEntry) throws IOException {
        File destFile = new File(destinationDir, zipEntry.getName());

        String destDirPath = destinationDir.getCanonicalPath();
        String destFilePath = destFile.getCanonicalPath();

        if (!destFilePath.startsWith(destDirPath + File.separator)) {
            throw new IOException();
        }
        return destFile;
    }
}
