package utils.csp;

import org.yaml.snakeyaml.Yaml;
import utils.YMLResourceUtils;

import java.io.InputStream;
import java.util.LinkedHashMap;
import java.util.Map;

public class CSPConfigUtils {

    private static final String UTILS_CONFIG_YML = "config/csp.utils.yaml";

    public Map<String, LinkedHashMap<String, String>> readConfig() {
        return new YMLResourceUtils().readConfig(UTILS_CONFIG_YML);
    }

}
