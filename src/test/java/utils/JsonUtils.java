package utils;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static utils.DateUtils.localDateTimeToString;

public abstract class JsonUtils {

    public static List<JSONObject> jsonArrayToJSONObjectList(JSONArray jsonArray){
        return IntStream.range(0, jsonArray.length())
                .mapToObj(i -> jsonArray.getJSONObject(i))
                .collect(Collectors.toList());
    }

    /**
     * Получить LocalDateTime из поля JSONObject в формате {@link DateFormat#DEFAULT_DATETIME_FORMAT}
     * по пути к данным полям {@link JsonUtils#getFieldValueByPathFromJson(String, String)}
     * @param jsonObject jsonObject с искомым полем
     * @param fieldPath путь с полю
     * @return значение поля с типом LocalDateTime
     */
    public static LocalDateTime getLocalDateTimeFromJSONObject(JSONObject jsonObject, String fieldPath){
        String localDateTimeStr = JsonUtils.getFieldValueByPathFromJson(fieldPath, jsonObject.toString());
        return DateUtils.stringToDateTime(localDateTimeStr);
    }

    /**
     * Получить List<LocalDateTime> из полей JSONArray в формате {@link DateFormat#DEFAULT_DATETIME_FORMAT}
     * по пути к данным полям {@link JsonUtils#getFieldValueByPathFromJson(String, String)}
     * @param jsonArray jsonArray с искомыми полями
     * @param fieldPath путь с полям
     * @return список значений полей с типом LocalDateTime
     */
    public static List<LocalDateTime> getLocalDateTimeListFromJSONArray(JSONArray jsonArray, String fieldPath){
        return jsonArrayToJSONObjectList(jsonArray).stream()
                .map(jsonObject -> getLocalDateTimeFromJSONObject(jsonObject, fieldPath))
                .collect(Collectors.toList());
    }

    /**
     * Получить List<String> из полей JSONArray по пути к данным полям {@link JsonUtils#getFieldValueByPathFromJson(String, String)}
     * @param jsonArray jsonArray с искомыми полями
     * @param fieldPath путь с полям
     * @return список значений полей в формате String
     */
    public static List<String> getStringListFromJSONArray(JSONArray jsonArray, String fieldPath){
        return jsonArrayToJSONObjectList(jsonArray).stream()
                .map(jsonObject -> getFieldValueByPathFromJson(fieldPath, jsonObject.toString(1)))
                .collect(Collectors.toList());
    }

    /**
     * Медод для поиска значений в json по указанному пути
     * Формат пути:
     * [3] - получить 4-й элемент массива
     * street.house[0].number - получить значение тега number у первого элемента массива house тега street
     *
     * @param fieldPath путь к тегу
     * @param json json для поиска
     * @return значение в формате String по указонному пути
     */
    public static String getFieldValueByPathFromJson(String fieldPath, String json) {
        try {
            return getFieldValueByPathFromJson(Arrays.asList(fieldPath.split("\\.")), json);
        } catch (JSONException e) {
            throw new JSONException("Path not found!!!  path: " + e.getMessage() + "\n json: " + json );
        }
    }

    private static String getFieldValueByPathFromJson(List<String> fieldPathList, String json) {
        List<String> copyPath = new ArrayList<>(fieldPathList);
        if (copyPath.size() == 0) {
            return json;
        }
        String fistPathItem = copyPath.remove(0);
        try {
            if (StringUtils.startsWith(fistPathItem, "[")) {
                return getFieldValueByPathFromJson(
                        copyPath,
                        new JSONArray(json).get(getIndex(fistPathItem)).toString());
            }
            JSONObject jsonObject = new JSONObject(json);
            if (fistPathItem.matches(".*\\[\\d+\\]")) {
                String tag = fistPathItem.substring(0, fistPathItem.indexOf('['));
                int index = getIndex(fistPathItem);
                return getFieldValueByPathFromJson(
                        copyPath,
                        jsonObject.getJSONArray(tag).get(index).toString());
            }
            return getFieldValueByPathFromJson(
                    copyPath,
                    jsonObject.get(fistPathItem).toString());
        } catch (JSONException e) {
            if (copyPath.size() == 0) {
                throw new JSONException(fistPathItem + "\nInner JSONException-> " + e.getMessage());
            } else {
                throw new JSONException(fistPathItem + "." + e.getMessage());
            }
        }
    }

    private static int getIndex(String itemPath) {
        return Integer.parseInt(StringUtils.substringBetween(itemPath, "[", "]"));
    }
}
