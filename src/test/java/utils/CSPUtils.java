package utils;

import org.apache.commons.exec.OS;
import ru.vtb.at.businessmodel.certificate.BrowserCertificate;
import utils.csp.CSPRootCert;
import utils.csp.CSPUserCert;

import java.io.IOException;

public class CSPUtils {

    /**
     * Метод устанавливает сертификат в систему.
     *
     * @param certTemplate = сертификат для установки
     * @throws IOException
     */
    public synchronized static void installCertificateFromDPIfNotExist(BrowserCertificate certTemplate) throws IOException {
        //Для операционных систем семейства Windows
        if (OS.isFamilyWindows()) {
            //Проверяем наличие корневого сертификата
            if (!CSPRootCert.isRootCertPresent()) {
                //Устанавливаем корневой сертификат
                CSPRootCert.getRootCAFromDP();
                CSPRootCert.installRootCert();
            }
            //Проверяем наличие контейнера с закрытым ключом
            if (!CSPUserCert.isCertificateInstalled(certTemplate.getCert_name())) {
                //Устанавливаем закртый ключ сертификат
                CSPUserCert.getUserCertFromDP(certTemplate);
                CSPUserCert.getUserPrivateKeyFromDP(certTemplate);
                CSPUserCert.installUserCert(certTemplate);
            }
        }
    }
}
