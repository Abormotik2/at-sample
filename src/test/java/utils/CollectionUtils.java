package utils;

import java.util.Collection;
import java.util.Iterator;
import java.util.Random;

public abstract class CollectionUtils {

    /**
     * Получить случайный элемент коллекции
     *
     * @param collection коллекция с икомыми элементами
     * @param <T>        тип элемента коллекции
     * @return случайный элемент коллекции
     */
    public static <T> T getRandom(Collection<T> collection) {
        int size = collection.size();
        if (size == 0) {
            return null;
        }
        Iterator<T> iterator = collection.iterator();
        int i = 0;
        int randomIndex = new Random().nextInt(collection.size());
        T element = null;
        while (iterator.hasNext()) {
            element = iterator.next();
            if (i == randomIndex) {
                break;
            }
            i++;
        }
        return element;
    }
}
