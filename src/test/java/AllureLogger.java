import hooks.Hooks;
import io.qameta.allure.listener.ContainerLifecycleListener;
import io.qameta.allure.listener.StepLifecycleListener;
import io.qameta.allure.listener.TestLifecycleListener;
import io.qameta.allure.model.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.vtb.at.exceptions.FrameworkRuntimeException;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;

public class AllureLogger implements StepLifecycleListener, TestLifecycleListener, ContainerLifecycleListener {

    private static final Logger LOGGER = LogManager.getLogger(AllureLogger.class.getSimpleName());


    @Override
    public void afterStepStop(StepResult result) {
        if (result.getDescription() != null && result.getDescription().contains("SoftAssert")) {
            LOGGER.info("Устанавливаем для шага: '" + result.getName() + "', статус=BROKEN");
            result.setStatus(Status.BROKEN);
            result.setDescription(result.getDescription().replace("SoftAssert:", ""));
        }
    }

    @Override
    public void beforeTestStop(TestResult result) {
        StatusDetails statusDetails = result.getStatusDetails();
        if (result.getStatus().equals(Status.FAILED)) {
            String message = "ОШИБКА ТЕСТА:\n\"" + statusDetails.getMessage().replaceFirst("\n", "") + "\"";
            String brokenStepsMessages = findBrokenStepsMessage(result.getSteps());
            if (brokenStepsMessages != null) {
                message = message + brokenStepsMessages;
            }
            statusDetails.setMessage(message);
            result.setStatusDetails(statusDetails);
        }
    }


    private String findBrokenStepsMessage(List<StepResult> stepResults) {
        ArrayList<StepResult> brokenSteps = stepResults.stream().filter(stepResult -> stepResult.getStatus().equals(Status.BROKEN)).collect(Collectors.toCollection(ArrayList::new));
        if (brokenSteps.size() > 0) {
            StringBuilder builder = new StringBuilder();
            builder.append("\n\n");
            builder.append("Другие поломки в тесте (BROKEN STEPS):");
            builder.append("\n[Также в тесте присутствуют ошибки типа SoftAssert : ");
            for (int i = 0; i < brokenSteps.size(); i++) {
                if (brokenSteps.get(i).getDescription() != null) {
                    builder.append("\n").append("Ошибка №:").append(i + 1).append("- ").append(brokenSteps.get(i).getDescription());
                } else {
                    builder.append("\n").append("Ошибка в шаге: ").append(brokenSteps.get(i).getName());
                }
            }
            builder.append("]");
            return builder.toString();
        }
        return null;
    }

    @Override
    public void beforeContainerStop(TestResultContainer container) {
        List<FixtureResult> hookSteps = new ArrayList<>(container.getAfters());
        hookSteps.removeIf(fixtureResult -> fixtureResult.getName().contains("afterStep"));
        container.setAfters(hookSteps);
    }

    @Override
    public void afterTestStart(TestResult result) {
        try {
            Properties properties = new Properties();
            Hooks.getEnvironment().forEach(properties::setProperty);
            Files.createDirectories(Paths.get("target/allure-results"));
            FileOutputStream fos = new FileOutputStream("target/allure-results/environment.properties");
            properties.store(fos,"Allure properties");
            fos.close();
        } catch (IOException e) {
            throw new FrameworkRuntimeException("Ошибка при создании файла target/allure-results/environment.properties", e);
        }
    }
}