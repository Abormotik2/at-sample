package hooks;

import com.consol.citrus.report.MessageTracingTestListener;
import com.epam.reportportal.service.ReportPortal;
import cucumber.api.*;
import cucumber.api.java.After;
import cucumber.api.java.AfterStep;
import cucumber.api.java.Before;
import extensions.AllureAtlasListener;
import extensions.ElementReadyClickSendKeysExtension;
import extensions.IsDisplayedExtension;
import gherkin.pickles.PickleCell;
import gherkin.pickles.PickleRow;
import gherkin.pickles.PickleString;
import gherkin.pickles.PickleTable;
import io.qameta.atlas.core.Atlas;
import ru.vtb.at.Config;
import ru.vtb.at.DataProviderUnlock;
import steps.BaseSteps;
import utils.allure.AllureHelper;

import java.lang.reflect.Field;
import java.nio.charset.StandardCharsets;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class Hooks extends BaseSteps {


    @Before
    public void setUp() {
        Atlas atlas = getAtlas();
        atlas.extension(new ElementReadyClickSendKeysExtension());
        atlas.extension(new IsDisplayedExtension());
        atlas.listener(new AllureAtlasListener());
        DataProviderUnlock.setUp();
    }
    public static Map<String, String> getEnvironment() {
        Map<String, String> environment = new HashMap<>();
        environment.put("browser", Config.getStringSystemProperty("browser"));
        environment.put("remote", Config.getStringSystemProperty("remote"));
        environment.put("proxy", Config.getStringSystemProperty("proxy"));
        return environment;
    }

    @After
    public void tearDown(Scenario scenario) {
        log.info("Finish scenario " + scenario.getName());
        String message = "Finish scenario";
        AllureHelper.attachTxt("Txt", message);
        if (driverIsActive()) {
            AllureHelper.attachPageSource(getDriver().getPageSource().getBytes(StandardCharsets.UTF_8));
            AllureHelper.attachScreenShot("Скриншот последней операции", getScreenShooter().takeScreenshot());
            shutdownDriver();
        }
        MessageTracingTestListener messageTracingTestListener = (MessageTracingTestListener) getEndpointByName("messageTracingTestListener");
        messageTracingTestListener.onTestFinish(getCitrusRunner().getTestCase());
        assertsManager.softAssert().assertAll();
        assertsManager.softAssert().flush();
        DataProviderUnlock.unlockResources();
    }

    @AfterStep
    public void afterStep(Scenario scn) throws NoSuchFieldException, IllegalAccessException {
        Field testCaseField = scn.getClass().getDeclaredField("testCase");
        Field stepResults = scn.getClass().getDeclaredField("stepResults");
        testCaseField.setAccessible(true);
        stepResults.setAccessible(true);

        TestCase tc = (TestCase) testCaseField.get(scn);
        List results = (List) stepResults.get(scn);
        Field testSteps = tc.getClass().getDeclaredField("testSteps");
        testSteps.setAccessible(true);

        List<TestStep> teststeps = tc.getTestSteps();
        if (teststeps.get(results.size() - 1) instanceof PickleStepTestStep) {
            PickleStepTestStep pts = (PickleStepTestStep) teststeps.get(results.size() - 1);
            String text = pts.getStepText();
            String arguments = "";
            for (int i = 0; i < pts.getStepArgument().size(); i++) {
                if (pts.getStepArgument().get(i) instanceof PickleString) {
                    arguments += ((PickleString) pts.getStepArgument().get(i)).getContent();
                } else {
                    PickleTable currentArgument = (PickleTable) pts.getStepArgument().get(i);
                    for (int j = 0; j < currentArgument.getRows().size(); j++) {
                        PickleRow row = currentArgument.getRows().get(j);
                        for (int k = 0; k < row.getCells().size(); k++) {
                            PickleCell cell = row.getCells().get(k);
                            arguments += cell.getValue() + " ";
                        }
                        arguments += "\n";
                    }
                }
            }
            if (!arguments.trim().isEmpty()) {
                text += "\n\nАргументы шага:\n" + arguments.trim();
            }
            Result.Type status = ((Result) results.get(results.size() - 1)).getStatus();
            if (status.equals(Result.Type.PASSED)) {
                ReportPortal.emitLog(text, "INFO", Calendar.getInstance().getTime());
            } else if (status.equals(Result.Type.SKIPPED) | status.equals(Result.Type.PENDING)) {
                ReportPortal.emitLog(text, "WARN", Calendar.getInstance().getTime());
            } else {
                ReportPortal.emitLog(text, "ERROR", Calendar.getInstance().getTime());
            }
        }
    }
}