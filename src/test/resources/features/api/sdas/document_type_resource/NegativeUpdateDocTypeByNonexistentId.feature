# language: ru

@Api
@sdas
Функция: Я, как сотрудник организации, хочу проверить ответ сервиса на обновленние типа документа с несуществующим id

  Предыстория:

    Дано сохраняю строку "0" в переменную "nonexistent_id"
    И сформирован json типа документа для сохранения в СДАС и сохранен в "test_doc_type"

  Сценарий: (Negative) Обновить тип документа с несуществующем id

    Когда я с ролью "сотрудник организации" отправляю запрос "обновить тип документа архивных систем по id" и сохраняю содержимое "body" ответа в "error_message", параметры запроса - body: "test_doc_type", pathParam:
      | id | nonexistent_id |
    Тогда поля json "error_message" соответстуют указанным строкам:
      | status  | 500                        |
      | error   | Internal Server Error      |
      | message | Entity with id=0 not found |

