# language: ru

@Api
@sdas
Функция: Я с ролью сотрудкик организации хочу получить страницу списка архивных документов отфильтрованных по дате документа от указанной даты включительно

  Предыстория:

    Дано сформирован список json документов для сохранения в СДАС в кол-ве "3" шт и сохранен в "test_migration_doc_list"
    И я с ролью "администратор" отправляю запрос "сохранить список документов архивных систем" и сохраняю содержимое "body" ответа в "doc_from_migration", параметры запроса - body: "test_migration_doc_list"
    И сохраняю случайное значение поля даты "documentDate" списка документов "test_migration_doc_list" в переменную "documentDateDateFromTest"

  Сценарий: Получить список документов с фильтром по documentDateDateFrom

    Когда я с ролью "сотрудник организации" отправляю запрос "получить страницу документов архивных систем" и сохраняю содержимое "body.data" ответа в "doc_page", параметры запроса - queryParam:
      | documentDateDateFrom | documentDateDateFromTest |
    Тогда список json "doc_page" не пустой
    Затем сохраняю список dataTime полей "documentDate" списка документов "doc_page" в переменную "documentDateList"
    И даты LocalDataTime списка "documentDateList" больше или равны даты String "documentDateDateFromTest"

