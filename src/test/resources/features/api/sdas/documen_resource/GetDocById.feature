# language: ru

@Api
@sdas
Функция: Я с ролью сотрудкик организации хочу получить детальную информацию архивного документа по его id

  Предыстория:

    Дано сформирован json документа для сохранения в СДАС и сохранен в "test_migration_doc"
    Дано я с ролью "администратор" отправляю запрос "сохранить документ архивных систем" и сохраняю содержимое "body" ответа в "doc_from_migration", параметры запроса - body: "test_migration_doc"
    И сохраняю содержимое поля "id" документа "doc_from_migration" в переменную "idTest"

  Сценарий: Получить документ по id

    Когда я с ролью "сотрудник организации" отправляю запрос "получить документ архивных систем по id" и сохраняю содержимое "body" ответа в "detail_doc_info", параметры запроса - pathParam:
      | id | idTest |
    Тогда поля json "detail_doc_info" соответстуют полям json "test_migration_doc"
      | documentNumber              |
      | documentDate                |
      | docTypeName                 |
      | orgName                     |
      | branchName                  |
      | sourceName                  |
      | signer                      |
      | content                     |
      | status.comment              |
      | status.extended             |
      | status.base                 |
      | clientId                    |
      | payerAccount                |
      | receiverBic                 |
      | receiverCorrAccount         |
      | attachments[0].attachmentId |
      | attachments[0].creationTime |
      | attachments[0].extension    |
      | attachments[0].fileName     |
      | attachments[0].hash         |
      | attachments[0].size         |
      | attachments[0].typeExtId    |

