# language: ru
# образец
@role-samples
@api-demo
Функционал: Чтобы продемонстировать механизм обновления прав необходимой Роли
            я, как инженер АТ, получаю данные из клиента Дата-провайдера, валидирую их,
            создаю необходимый шаблон (без прав) и роль,
            назначаю роль организации и валидирую ее,
            после чего выполняю обновление прав и связывание

  Предыстория: В Дата-провайдере хранятся консистентные данные как минимум одной организации.

  Сценарий: Обновление Роли организации (добавление прав)

    Пусть я получил случайную организацию

    Также я получил случайного пользователя

    # создаю роль на основе "пустого" (без прав) шаблона роли
    # обновляю права роли
    # привязка роли организации пользователю

# на этом шаге на основе "пустого" шаблона создается роль с отсутствием каких-либо прав
    И создал в ДБО Роль "Тестовая Роль 101" на основе нового Шаблона "Тестовый Шаблон 101" для полученной организации

    Затем обновил Роль "Тестовая Роль 101" следующими правами:
      | Продукт | Объект  | Операция |
      | Выписки | Выписка | Просмотр |
      | Выписки | Выписка | Экспорт  |

    И привязал пользователя к полученной организации с ролью "Тестовая Роль 101"

    И создал связь между организацией и пользователем